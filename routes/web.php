<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::group(['middleware' => 'auth'], function () {
Route::get('/', 'DashboardController@index');
Route::get('/home', 'HomeController@index');
Route::resource('categories','ProductCategoryController');
Route::resource('types','ProductTypeController');
Route::resource('products','ProductController',['only'=>['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']]);
Route::get('/import_product','ProductController@indexImport');
Route::post('/proses_import','ProductController@prosesImport');
Route::get('product-number','ProductController@sizeNumber');
Route::get('product-size','ProductController@size');
Route::get('product-types','ProductController@type');
Route::get('payment-types','ProductController@payment');    
Route::get('product-category','ProductController@productCategory');
Route::resource('customers','CustomerController',['only'=>['index', 'show', 'edit', 'create', 'store', 'update', 'destroy']]);
Route::get('customers/edit/{id}', 'CustomerController@edit');
Route::resource('receipts', 'ReceiptController',['only'=>['index', 'show', 'create', 'store']]);
Route::get('/loadProducts','ReceiptController@loadProduct');
Route::get('/addOrder','ReceiptController@addOrder');

Route::resource('sales_orders','SalesOrderController',['only'=>['index', 'show', 'create', 'store']]);
Route::get('/findCustomer', 'SalesOrderController@findCustomer' );
Route::get('/loadProductSales', 'SalesOrderController@loadProductSales');
Route::get('/addSalesOrder', 'SalesOrderController@addOrder');
Route::resource('users','UserController');
Route::resource('roles','RoleController');
Route::resource('report_so','ReportSoController');
Route::get('report_so_daily','ReportSoController@salesOrderDaily');
Route::get('roles/edit/{id}', 'RoleController@edit');
Route::get('/report_by_customer', 'CustomerController@reportCustomer');
Route::get('/report_by_po','ProductController@receiptWeek');
Route::get('/report_by_sell','ProductController@sellWeek');
Route::get('/report_so_by_range', 'ReportSoController@index');
Route::resource('report_stock','StockProductController');
Route::get('/export_product','StockProductController@exportExcelProduct');
Route::get('/report_mounth','ReportSoController@salesOrderMounth');
Route::get('/print_data_report','ReportSoController@printDataSellout');
});
