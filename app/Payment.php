<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['name'];

    public function sales_order(){
        return $this->hasMany('App\SalesOrder');
    }
}
