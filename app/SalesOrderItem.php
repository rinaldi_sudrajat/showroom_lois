<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
class SalesOrderItem extends Model
{
    protected $fillable = ['sales_order_id','product_id','qty','price'];

    public function sales_order()
    {
        return $this->belongsTo('App\SalesOrder');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    
    public function subtotal()
    {
      $subtotal = $this->qty * $this->price; 
      return $subtotal;
    }

  public function setStock()
  {
    $this->product->qty = ($this->product->qty - $this->qty);
    $this->product->save(); 
    return $this->product->save();
  }

  public function scopeSalesOrderMounth($query,$month=null){
      $array_data =[];
      $months = $month ? Carbon::parse($month)->month : Carbon::now()->month;
      $years = $month ? Carbon::parse($month)->year : Carbon::now()->year;

      $data = $query
                ->join('products','products.id','=','sales_order_items.product_id')
                ->join('sales_orders', 'sales_orders.id', '=', 'sales_order_items.sales_order_id')
                ->select(DB::raw('FLOOR((DAYOFMONTH(sales_orders.sales_date)-1)/7) + 1 as week,
                          SUM(sales_order_items.qty) as total_qty,
                          SUM(sales_order_items.price) as total_price'))
                ->where(DB::raw('MONTH(sales_orders.sales_date)'), '=', $months)
                ->where(DB::raw('YEAR(sales_orders.sales_date)'), '=', $years)
                ->groupBy('week')
                ->orderBy('week')
                ->get();

      return $data;
  }

}
