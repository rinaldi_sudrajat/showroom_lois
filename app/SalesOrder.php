<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
class SalesOrder extends Model
{
    protected $fillable = ['sales_number','sales_date','customer_id','user_id','payment_id','edc_payment_id','description','customer_value','discount_value'];

    
  public function order_items()
  {
    return $this->hasMany('App\SalesOrderItem', 'sales_order_id');
  }

  public function customer()
  {
    return $this->belongsTo('App\Customer','customer_id');
  }

  public function user()
  {
    return $this->belongsTo('App\User','user_id');
  }

  public function edc(){
    return $this->belongsTo('App\EdcPayment','edc_payment_id');
  }

  public function payment(){
    return $this->belongsTo('App\Payment','payment_id');
  }

  public function products()
  {
    return $this->hasManyThrough('App\Product', 'App\SalesOrderItem');
  }

  public function total()
  {
    $array=array();
    foreach($this->order_items as $order){
      array_push($array, $order->subtotal());
    }
    return (array_sum($array));
  }

  public function grand_total()
  {
    return ($this->total() - $this->discount_value - $this->customer_value);
  }

  
}
