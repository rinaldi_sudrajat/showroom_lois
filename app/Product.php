<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    protected $fillable = ['product_type_id','product_size_id','product_size_number_id','product_category_id','name','price_list','sale_price','qty','picture','active'];


    public function type(){
    	return $this->belongsTo('App\ProductType','product_type_id');
    }

    public function size(){
    	return $this->belongsTo('App\ProductSize','product_size_id');
    }

    public function category(){
    	return $this->belongsTo('App\ProductCategory','product_category_id');
    }


    public function number(){
        return $this->belongsTo('App\ProductSizeNumber','product_size_number_id');
    }

    public function receipt()
    {
        return $this->belongsToMany('App\Receipt','receipt_items');
    }

    public function receipt_items()
    {
        return $this->hasMany('App\ReceiptItem');
    }

    public function sales_order_items()
    {
        return $this->hasMany('App\SalesOrderItem');
    }

    public function setProductType($request)
    {
    $product_type = ProductType::where('name', $request->input('product_type_id',false))->get()->first();
    if(is_null($product_type)){
      $product_type = ProductType::create(['name'=>$request->input('product_type_id',false)]);
    }
    return $product_type->id;
    }


    public function setProductSize($request)
    {
    $product_size = ProductSize::where('name', $request->input('product_size_id',false))->get()->first();
    if(is_null($product_size)){
      $product_size = ProductSize::create(['name'=>$request->input('product_size_id',false)]);
    }
    return $product_size->id;
    }

    public function setProductNumber($request)
    {
    $product_number = ProductSizeNumber::where('name', $request->input('product_size_number_id',false))->get()->first();
    if(is_null($product_number)){
      $product_number = ProductSizeNumber::create(['name'=>$request->input('product_size_number_id',false)]);
    }
    return $product_number->id;
    }

    public function setImportCategory($value)
    {
        $categories = ProductCategory::where('name', $value->category)->get()->first();
        if(is_null($categories)){
            $categories = ProductCategory::create(['name'=> $value->category]);
        }
        return $categories->id;
    }

    public function setImportType($value)
    {
        $type = ProductType::where('name', $value->type)->get()->first();
        if(is_null($type)){
            $type = ProductType::create(['name'=>$value->type]);
        }
        return $type->id;
    }

    public function setImportSize($value)
    {
        $sizes = ProductSize::where('name', $value->size)->get()->first();
        if(is_null($sizes)){
            $sizes = ProductSize::create(['name'=>$value->size]);
        }
        return $sizes->id;
    }

    public function setImportSizeNumber($value)
    {
        $size_numbers = ProductSizeNumber::where('name', $value->size_number)->get()->first();
        if(is_null($size_numbers)){
            $size_numbers = ProductSizeNumber::create(['name'=>$value->size_number]);
        }
        return $size_numbers->id;
    }

    public function historyReceipt($start_date = null, $end_date = null)
    {
        return $this->receipt_items->where('receipt_order.receipt_date', ">=", $start_date)->where('receipt_order.receipt_date', "<=", $end_date);
    }

    public function historySo($start_date = null, $end_date = null)
    {
     return $this->sales_order_items->where('sales_order.sales_date', ">=", $start_date)->where('sales_order.sales_date', "<=", $end_date);
    }


}
