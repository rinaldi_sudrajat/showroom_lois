<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $fillable = ['receipt_number','po_number','receipt_date','receipt_desc','order_by'];

     public function receipt_items()
    {
        return $this->hasMany('App\ReceiptItem', 'receipt_id');
    }

    public function products()
    {
        return $this->hasManyThrough('App\Product', 'App\ReceiptItem');
    }

    public function user()
    {
        return $this->belongsTo('App\User','order_by');
    }
}
