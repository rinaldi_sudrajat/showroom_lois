<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['customer_number','full_name','dob','address','phone','email','point_rewards','active'];


    public $rules = 
    [
        'full_name' => 'required',
        'address' => 'required',
        'phone' => 'required|unique:customers,phone'	           
    ];

	public function sales_orders(){
        return $this->hasMany('App\SalesOrder');
    }

    public function sales_order_items(){
        return $this->hasManyThrough('App\SalesOrderItem', 'App\SalesOrder');
    }

	public function montlyReport($start_date = null, $end_date = null)
    {
        return $this->sales_order_items->where('sales_order.sales_date', ">=", $start_date)->where('sales_order.sales_date', "<=", $end_date);
    }
}
