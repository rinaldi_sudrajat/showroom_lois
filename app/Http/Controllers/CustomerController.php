<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Validator;
use Flash;
class CustomerController extends Controller
{
    public function index(Request $request){
   	$customers = Customer::where('active',true)->get();
    	if($request->ajax()) {
      		return response()->json(['aaData' => $customers]);
    	}
    	return view('customers.index')->with('customers', $customers);
    }


    public function show(Request $request, $id){
            if($request->ajax())
      {
        $customer = Customer::where('phone_number', $request->phone_number);
        if(customer)
        {
          return response()->json(['customer'=>$customer]);
        }else{
          $html = view("sales_orders.addCustomer")->renderSections();
          return response()->json(['html' => $html]);
        }
      }else{
        $customers = Customer::find($id);
        return view('customers.show',compact('customers'));
      } 
    }

    public function create(){
    	 return view('customers.create');
    }

    public function store(Request $request){
    	$Customer = new Customer;
      $validator = Validator::make($request->all(),$Customer->rules);

      if ($validator->fails()){
        if($request->ajax()){
          return response()->json(['error'=>"Customer already exist"]);
        }else{
          return view('customers.create')->withErrors($validator);
        }
      }

      $customer = Customer::create([
        'customer_number'=> "COS-".mt_rand(),
        'full_name' => $request->input('full_name'),
        'address' => $request->input('address'),
        'city' => $request->input('city'),
        'phone' => $request->input('phone'),
        'email' => $request->input('email'),
        'point_rewards' => 0,
        'active' => true,
      ]);

      flash('Customer was successfully created', 'success');
      if($request->ajax()){
          return response()->json(['customer'=>$customer]);
      }else{
        return redirect('customers');
      }
    }

    public function edit($id){
    	$data = Customer::findOrFail($id);
      return view('customers/edit',compact('data','status'));
    }

    public function update(Request $request, $id){
    	      $data = Customer::findOrFail($id);
      $data->update($request->all());
      return redirect()->route('customers.index');
    }

    public function destroy(Request $request){
      $product = Customer::find($request->id);
      if($request->ajax())
      {
        $product->update(['active'=>false]);
        return response()->json(['status' => false]);
      }else{
        return redirect('customers');
      }
    }


    public function reportCustomer(Request $request)
    {
      if($request->ajax())
        dd($request->id);
        $start_date = $request->from_date;
        $end_date = $request->to_date;
        $customer = Customer::find($request->id);
        $trans = $customer->montlyReport($start_date,$end_date);
        $html = view("customers.include.report_by_customer", compact('trans'))->renderSections();
          return response()->json(['html' => $html]);
      return view('customers.include.report_by_customer')->with('transcation',$transcation);
    } 
}
