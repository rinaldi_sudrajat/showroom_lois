<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Customer;
use App\SalesOrder;
use App\Receipt;
class DashboardController extends Controller
{

	public function __construct()
  	{
    	$this->middleware('auth');
  	}
    public function index(){
    	$count_product = Product::count();
    	$count_costumers = Customer::count();
    	$count_sales = SalesOrder::count();
    	$count_receipt = Receipt::count(); 
   		return view('dashboard.index',compact('count_product','count_costumers','count_sales','count_receipt'));
    }
}
