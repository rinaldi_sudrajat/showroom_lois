<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use PDF;
class StockProductController extends Controller
{
	public function __construct()
  	{
   	 	$this->middleware('auth');
  	}

    public function index(){
    	$product = Product::with('category')->get();
    	return view('report_stock.index',compact('product'));
    }


    public function exportExcelProduct(){
    	$product = Product::with('category')->get();
      view()->share('product',$product);
      $pdf = PDF::loadView('report_stock.downloads.excel_product');
      return $pdf->setPaper('a4', 'landscape')->download('data.pdf');
    }
}
