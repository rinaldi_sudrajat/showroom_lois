<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductType;
class ProductTypeController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request){
    	 $types = ProductType::all();
       	if($request->ajax()) {
            return response()->json(['aaData' => $types]);
        }
        return view('type.index')->with('types', $types);
    }

    public function show($id){
        $type = ProductType::find($id);
        return view('type.show',compact('type'));   
    }
}
