<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use DB;
use App\User;
class RoleController extends Controller
{
    public function index(Request $request){
        $roles = Role::all();
           	if($request->ajax()) {
      		return response()->json(['aaData' => $roles]);
    	}
    	return view('roles.index')->with('roles', $roles);
    }


    public function show(){
        
    }

    public function create(){
        $permission = Permission::get();
        return view('roles.create',compact('permission'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'display_name' => 'required',
            'description' => 'required',
            'permission' => 'required',
        ]);

        $role = new Role();
        $role->name = $request->input('name');
        $role->display_name = $request->input('display_name');
        $role->description = $request->input('description');
        $role->save();

        foreach ($request->input('permission') as $key => $value) {
            $role->attachPermission($value);
        }
        
        return redirect('roles');
    }

    public function edit($id){
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("permission_role")->where("permission_role.role_id",$id)
            ->pluck('permission_role.permission_id','permission_role.permission_id')->toArray(); ;
        return view('roles.edit',compact('role','permission','rolePermissions'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'display_name' => 'required',
            'description' => 'required',
            'permission' => 'required',
        ]);

        $role = Role::find($id);
        $role->display_name = $request->input('display_name');
        $role->description = $request->input('description');
        $role->save();

        DB::table("permission_role")->where("permission_role.role_id",$id)
            ->delete();

        foreach ($request->input('permission') as $key => $value) {
            $role->attachPermission($value);
        }
        return redirect('roles');
    }

    public function destroy(Request $request){
    $product = Role::find($request->id);
      if($request->ajax())
      {
        Role::destroy($request->id);
      }else{
        return redirect('roles');
      }
    }
}
