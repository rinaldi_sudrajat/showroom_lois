<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseOrder;
use App\PurchaseOrderItems;
use App\Product;
use Auth;
class PurchaseOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
    $purchase_orders = PurchaseOrder::orderBy('order_date', 'DESC')->get();
    if($request->ajax()) {
        return response()->json(['aaData' => $purchase_orders]);
        }
        return view('purchase_orders.index')->with('purchase_orders', $purchase_orders);
    }

    public function show(){

    }

    public function create(){
    $products = Product::all();
    return view('purchase_orders.create',compact('products'));
    }

    public function addOrder(Request $request)
    {
    if($request->ajax()){
      if(!is_null($request->filter)){
        $products = Product::whereNotIn("id",$request->product_id)->get();
        $html = view("purchase_orders.include.products", compact('products'))->renderSections();
      }else{
        $products = Product::whereIn("id", $request->product_id)->get();
        $html = view("purchase_orders.include.addOrder", compact('products'))->renderSections();
      }
      
      if($products)
      {
        $html = $html;
      }else{
        $html = null;
      }
      return response()->json(['html' => $html]);
    }else{
      $purchase_order = PurchaseOrder::find($id);
      return view('purchase_orders.show')->with('purchase_order', $purchase_order);
    }
  }

    public function loadProduct(Request $request)
    {
    if($request->ajax()){
      if(!is_null($request->filter)){
        $products = Product::whereNotIn("id",$request->product_id)->get();
        $html = view("purchase_orders.include.products", compact('products'))->renderSections();
      }else{
        $products = Product::whereIn("id", $request->product_id)->get();
        $html = view("purchase_orders.include.addOrder", compact('products'))->renderSections();
      }
      
      if($products)
      {
        $html = $html;
      }else{
        $html = null;
      }
      return response()->json(['html' => $html]);
    }else{
      $purchase_order = PurchaseOrder::find($id);
      return view('purchase_orders.show')->with('purchase_order', $purchase_order);
    }
  }

    public function store(Request $request){
    if($request->ajax())
    {

      $order = new PurchaseOrder($request->input('order'));
      $order->user_id = Auth::user()->id;
      $order->order_number = "PO-".mt_rand();
      if($order->save())
      {
        foreach($request->input('items')['product_id'] as $key=>$item)
        {
          PurchaseOrderItems::create([
            "purchase_order_id" => $order->id,
            "product_id" => $item,
            "qty" => $request->input('items')['qty'][$key], 
          ]);
        }

        if($order->order_items)
        {
          foreach($order->order_items as $order_item)
          {
            $productItem = Product::where(['id'=>$order_item->product_id,])->first();
            if(is_null($productItem))
            {
              $order->product()->save(new productItem([
                'product_id'=>$order_item->product_id, 
                'minimal_qty'=>1, 
                'qty'=>$order_item->setStock(), 
              ]));
            }else{
              $productItem->update([ 
                'qty'=>($order_item->qty + $productItem->qty), 
                'sale_price_discount'=>0
              ]);
              $order_item->setStock();
            }
          }
        }
        return response()->json(['order'=>$order->order_items, 'status'=>'success']);
      }else{
        return response()->json(['order'=>null, 'status'=>'failed']);
      }
    }
  }


}
