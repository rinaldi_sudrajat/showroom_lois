<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalesOrder;
use App\SalesOrderItem;
use App\Product;
use App\Payment;
use App\Customer;
use Auth;
use App\EdcPayment;
class SalesOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $sales_orders = SalesOrder::OrderBy('sales_date','DESC')->get();
        if($request->ajax()) {
            return response()->json(['aaData' => $sales_orders]);
        }
        return view('sales_orders.index')->with('sales_orders', $sales_orders);
    }

    public function show($id){
      $sales_order = SalesOrder::find($id);
      return view('sales_orders.show',compact('sales_order'));
    }

    public function create(){
        $product = Product::all();
        $customer = Customer::pluck('full_name','id');
        $payment = Payment::pluck('name','id');
        return view('sales_orders.create',compact('product','customer','payment')); 
    }

    public function findCustomer(Request $request)
    {
    if($request->ajax())
    {
      $customer = Customer::where('phone', $request->phone_number)->get();
      if(count($customer) > 0)
      {
        return response()->json(['customer'=>$customer]);
      }else{
        $phone = $request->phone_number;
        $html = view("sales_orders.addCustomer", compact('phone'))->renderSections();
        return response()->json(['html' => $html, 'customer' => []]);
      }
    }
  }

    public function loadProductSales(Request $request)
    {
    if($request->ajax()){
      if(Auth::user()->stores){
        $data = [];
        foreach(Auth::user()->my_products() as $product)
        {
          array_push($data, $product->id);
        }
        $products = Product::whereIn('products.id', array_values($data))->whereNotIn("products.id",$request->product_id)->get();
      }else{
        $products = Product::whereNotIn("products.id",$request->product_id)->get();
      }
      $html = view("sales_orders.products", compact('products'))->renderSections();
      return response()->json(['html' => $html]);
    }
  }

  public function addOrder(Request $request)
  {
    if($request->ajax()){
      if(!is_null($request->filter)){
        $products = Product::whereNotIn("products.id",$request->product_id)->whereIn()->get();
        $html = view("sales_orders.products", compact('products'))->renderSections();
      }else{
        $products = Product::whereIn("id", $request->product_id)->get();
        $html = view("sales_orders.addOrder", compact('products'))->renderSections();
      }
      
      if($products)
      {
        $html = $html;
      }else{
        $html = null;
      }
      return response()->json(['html' => $html]);
    }
  }

  public function store(Request $request)
  {    
    if($request->ajax())
      {
        $customer = Customer::where('phone',$request->input('order')['customer_id'])->first();
        $payment = Payment::where('name',$request->input('order')['payment_id'])->first();
        $edc_payment = EdcPayment::where('name',$request->input('order')['edc_payment_id'])->first(); 
        $order = new SalesOrder($request->input('order'));
        $order->user_id = Auth::user()->id;
        $order->sales_number = "SO-".mt_rand();
        $order->edc_payment_id = empty($request->input('order')['edc_payment_id']) ? 0 : $edc_payment->id;
        $order->customer_id = $request->input('order')['customer_id'];
        $order->customer_value = $request->input('order')['customer_value'];
        $order->discount_value = $request->input('order')['discount_value'];
  
        if($order->save())
        {
          foreach($request->input('items')['product_id'] as $key=>$item)
          {
            
            $order->order_items()->save(new SalesOrderItem([
              "product_id" => $item,
              "qty" => $request->input('items')['qty'][$key], 
              "price" => floatval($request->input('items')['price'][$key])
            ]));
          }
          
          if($order->order_items)
          {
            foreach($order->order_items as $order_item)
            {
                $order_item->product->update(['qty'=>($order_item->product->qty - $order_item->qty)]);
            }
          }
          return response()->json(['order'=>$order->order_items, 'status'=>'success']);
        }else{
          return response()->json(['order'=>null, 'status'=>'failed']);
        }
      }
  }

}
