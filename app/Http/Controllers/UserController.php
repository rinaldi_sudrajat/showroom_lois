<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\Role;
use Hash;
use DB;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $data = User::all();
        return view('user.index',compact('data'));
    }

    public function create(){
        $roles = Role::pluck('display_name','id');
        return view('user.create',compact('roles'));
    }

    public function store(){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        // return $request->all();


        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect('users');
    }

    public function edit($id){
        $user = User::find($id);
        $roles = Role::pluck('display_name','id');
        $userRole = $user->roles->pluck('id','id')->toArray();

        return view('user.edit',compact('user','roles','userRole'));
    }

    public function update(Request $request,$id){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('role_user')->where('user_id',$id)->delete();

        
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect('users');
    }


    public function destroy($id){
    $user = User::find($id);
    if($user) {
      User::destroy($id);
      return redirect('users');
    } else {
      return redirect('users');
    }
    }
}
