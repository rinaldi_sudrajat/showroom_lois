<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use App\ProductSizeNumber;
use App\ProductType;
use App\ProductSize;
use App\EdcPayment;
use Input;
use Flash;
use Excel;
class ProductController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

   public function index(Request $request){
  
      $products = Product::with('type','category','size','number')->where('active','=',true)->get();
    if($request->ajax()) {
      return response()->json(['aaData' => $products]);
    }
   return view('products.index')->with('products', $products);
      
   }

   public function create(){
    $category = ProductCategory::pluck('name','id');
    return view('products.create',compact('category'));
   }

   public function show($id){
    $product = Product::find($id);
    return view('products.show',compact('product'));
   }

   public function store(Request $request){
      if(Input::hasFile('picture')){
      $extension = Input::file('picture')->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension;
      $destinationPath = "uploads/products";
      $picture = Input::file('picture')->move($destinationPath, $fileName);
    }else{
      $picture = "uploads/products/default.jpg";
    }

    $product = new Product;
    $save_product = Product::create([
      'product_category_id' => $request->input('product_category_id'),
      'name' => $request->input('name'),
      'product_type_id' => is_null($request->input('product_type_id')) ? 1 : $product->setProductType($request),
      'product_size_id' => is_null($request->input('product_size_id')) ? 1 : $product->setProductSize($request),
      'product_size_number_id' => is_null($request->input('product_size_number_id')) ? 1 : $product->setProductNumber($request),
      'sale_price' => $request->input('sale_price',false),
      'picture' => $picture,
      'active' => true
      ]
    );
    flash('Product was successfully created', 'success');
    return redirect('products');
   }

   public function edit($id){
      $category = ProductCategory::pluck('name','id');
      $product = Product::findOrFail($id);
      return view('products.edit',compact('product', 'category'));
   }

   public function update($id, Request $request){
      $product = Product::findOrFail($id);

    if(Input::hasFile('picture')){
      $extension = Input::file('picture')->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension;
      $destinationPath = "uploads/products";
      $picture = Input::file('picture')->move($destinationPath, $fileName);
    }else{
      $picture = $product->picture;
    }

    $product->update([
      'product_category_id' => $request->input('product_category_id'),
      'name' => $request->input('name'),
      'product_type_id' => is_null($request->input('product_type_id')) ? 1 : $product->setProductType($request),
      'product_size_id' => is_null($request->input('product_size_id')) ? 1 : $product->setProductSize($request),
      'product_size_number_id' => is_null($request->input('product_size_number_id')) ? 1 : $product->setProductNumber($request),
      'sale_price' => $request->input('sale_price',false),
      'picture' => $picture,
      'active'  => true,
      ]
    );
    flash('Product was successfully updated', 'success');
    return redirect('products');
   }


   public function destroy(Request $request)
   {
      $product = Product::find($request->id);
         if($request->ajax())
            {
               $product->update(['active'=>false]);
               return response()->json(['status' => true]);
            }
         return redirect('products');
   }
     

  public function productCategory(Request $request)
  {
    $category = ProductCategory::find($request->cat);
    if($request->ajax()){
      $html = view("products.include.".strtolower($category->name))->renderSections();
      return response()->json(['html' => $html]);
    }
  }


   public function sizeNumber(Request $request)
   {
    if($request->ajax())
    {
      $numbers = ProductSizeNumber::where('name', 'LIKE', '%'.$request->input('term').'%')->get();
      $value = array();
      foreach($numbers as $number){
        array_push($value, $number->name);
      }
      return response()->json($value);
    }else{
      $number = ProductSizeNumber::find($request->id);
      return view('products.number')->with('number', $number);
    }
  }

  public function type(Request $request)
   {
    if($request->ajax())
    {
      $types = ProductType::where('name', 'LIKE', '%'.$request->input('term').'%')->get();
      $value = array();
      foreach($types as $type){
        array_push($value, $type->name);
      }
      return response()->json($value);
    }else{
      $type = ProductType::find($request->id);
      return view('products.type')->with('type', $type);
    }
  }

    public function payment(Request $request)
   {
    if($request->ajax())
    {
      $payment = EdcPayment::where('name', 'LIKE', '%'.$request->input('term').'%')->get();
      $value = array();
      foreach($payment as $payments){
        array_push($value, $payments->name);
      }
      return response()->json($value);
    }else{
      $payment = EdcPayment::find($request->id);
      return view('products.type')->with('payment', $payments);
    }
  }

  public function size(Request $request)
   {
    if($request->ajax())
    {
      $sizes = ProductSize::where('name', 'LIKE', '%'.$request->input('term').'%')->get();
      $value = array();
      foreach($sizes as $size){
        array_push($value, $size->name);
      }
      return response()->json($value);
    }else{
      $size = ProductSize::find($request->id);
      return view('products.size')->with('size', $size);
    }
  }

  public function receiptWeek(Request $request)
  {
    if($request->ajax())
      $start_date = $request->from_date;
      $end_date   = $request->to_date;
      $product  = Product::find($request->id);
      $trans  = $product->historyReceipt($start_date,$end_date);
      $html = view("products.report.report_receipt", compact('trans'))->renderSections();
        return response()->json(['html' => $html]);
    return view("products.report.report_receipt")->with('transcation',$transcation);
  }

  public function sellWeek(Request $request)
  {
    if($request->ajax())
      $start_date = $request->from_date;
      $end_date   = $request->to_date;
      $product  = Product::find($request->id);
      $trans  = $product->historySo($start_date,$end_date);
      $html = view("products.report.report_sellout", compact('trans'))->renderSections();
        return response()->json(['html' => $html]);
    return view("products.report.report_sellout")->with('transcation',$transcation);
  }

  public function indexImport(){
    return view('products.import');
  }


  public function prosesImport(Request $request){
      if(Input::hasFile('picture')){
      $extension = Input::file('picture')->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension;
      $destinationPath = "uploads/products";
      $picture = Input::file('picture')->move($destinationPath, $fileName);
    }else{
      $picture = "uploads/products/default.jpg";
    }

     if(Input::hasFile('excel')){
            $path = Input::file('excel')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();


        if(!empty($data) && $data->count()){
            foreach ($data as $key => $rows) {
                        $product = new Product;
                            Product::create([
                                'product_category_id' => $product->setImportCategory($rows),
                                'product_type_id' => $product->setImportType($rows),
                                'product_size_id' => $product->setImportSize($rows),
                                'product_size_number_id' => $product->setImportSizeNumber($rows),
                                'name' => $rows->artikel,
                                'price_list' => $rows->price_list,
                                'sale_price' => $rows->sale_price,
                                'qty' => 0,
                                'picture' => $picture,
                                'active' => 1 
                            ]);
                    }
                    return redirect('products');
                }
            }
            return back();
    }


}
