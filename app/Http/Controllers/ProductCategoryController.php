<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;

class ProductCategoryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request){
	   $categories = ProductCategory::all();
       	if($request->ajax()) {
            return response()->json(['aaData' => $categories]);
        }
        return view('categories.index')->with('categories', $categories);

    }

    public function show($id){
        $category = ProductCategory::find($id);
        return view('categories.show',compact('category'));
    }
}
