<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalesOrderItem;
use App\Product;
use App\Payment;
use App\ProductCategory;
use DB;
class ReportSoController extends Controller
{
    public function index(Request $request){
			$start_date = $request->from_date;
    		$end_date = $request->to_date;
            $payment_type = $request->payment_id;
            $category_type = $request->category_id;
            $category = ProductCategory::pluck('name','id');
    		$payments = Payment::pluck('name','id');
            $sales_order = DB::table('sales_orders')
            ->join('sales_order_items','sales_order_items.sales_order_id','=','sales_orders.id')
            ->join('products','products.id','=','sales_order_items.product_id')
            ->join('product_categories','product_categories.id','products.product_category_id')
            ->join('payments','sales_orders.payment_id','=','payments.id')
            ->select('payments.name as payment_name','sales_orders.sales_date','sales_order_items.qty','sales_order_items.price','sales_orders.customer_value','sales_orders.discount_value')
            ->where('product_categories.id','=',$category_type)
            ->where('sales_orders.payment_id','=',$payment_type)
            ->whereBetween('sales_orders.sales_date',[$start_date, $end_date])
        	->get();
    		return view('report.index',compact('sales_order','payments','category'));
    }


    public function printDataSellout(Request $request){
        $start_date = $request->from_date;
        $end_date = $request->to_date;
        $payment_type = $request->payment;
        $category_type = $request->category;
        $sales_order = DB::table('sales_orders')
        ->join('sales_order_items','sales_order_items.sales_order_id','=','sales_orders.id')
        ->join('products','products.id','=','sales_order_items.product_id')
        ->join('product_categories','product_categories.id','products.product_category_id')
        ->join('payments','sales_orders.payment_id','=','payments.id')
        ->select('payments.name as payment_name','sales_orders.sales_date','sales_order_items.qty','sales_order_items.price','sales_orders.customer_value','sales_orders.discount_value')
        ->where('product_categories.id','=',$category_type)
        ->where('sales_orders.payment_id','=',$payment_type)
        ->whereBetween('sales_orders.sales_date',[$start_date, $end_date])
        ->get();
        return view('report.downloads.print',compact('sales_order'));
    }

    public function salesOrderMounth(Request $request){
        $start_date = $request->from_date;
        $data_mounth = SalesOrderItem::SalesOrderMounth($start_date);
        // dd($data_mounth);
        return view('report.mounth',compact('data_mounth'));
    }



}
