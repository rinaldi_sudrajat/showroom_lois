<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Receipt;
use App\ReceiptItem;
use Auth;
class ReceiptController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
    $receipts = Receipt::orderBy('receipt_date', 'DESC')->get();
    if($request->ajax()) {
        return response()->json(['aaData' => $receipts]);
        }
        return view('receipts.index')->with('receipts', $receipts);
    }

    public function create(){
    $products = Product::all();
    return view('receipts.create',compact('products'));
    }


    public function show($id){
      $data = Receipt::find($id);
      return view('receipts.show',compact('data'));
    }

    public function addOrder(Request $request)
    {
    if($request->ajax()){
      if(!is_null($request->filter)){
        $products = Product::whereNotIn("id",$request->product_id)->get();
        $html = view("receipts.include.products", compact('products'))->renderSections();
      }else{
        $products = Product::whereIn("id", $request->product_id)->get();
        $html = view("receipts.include.addproducts", compact('products'))->renderSections();
      }
      
      if($products)
      {
        $html = $html;
      }else{
        $html = null;
      }
      return response()->json(['html' => $html]);
    }else{
      $receipts = Repeipt::find($id);
      return view('receipts.show')->with('receipts', $receipts);
    }
  }


  public function loadProduct(Request $request)
    {
    if($request->ajax()){
      if(!is_null($request->filter)){
        $products = Product::whereNotIn("id",$request->product_id)->get();
        $html = view("receipts.include.products", compact('products'))->renderSections();
      }else{
        $products = Product::whereIn("id", $request->product_id)->get();
        $html = view("receipts.include.addproducts", compact('products'))->renderSections();
      }
      
      if($products)
      {
        $html = $html;
      }else{
        $html = null;
      }
      return response()->json(['html' => $html]);
    }else{
      $receipts = Receipt::find($id);
      return view('receipts.show')->with('receipts', $receipts);
    }
  }

   public function store(Request $request){
    if($request->ajax())
    {

      $order = new Receipt($request->input('order'));
      $order->order_by = Auth::user()->id;
      $order->receipt_number = "GP-".mt_rand();
      if($order->save())
      {
        foreach($request->input('items')['product_id'] as $key=>$item)
        {
          ReceiptItem::create([
            "receipt_id" => $order->id,
            "product_id" => $item,
            "qty" => $request->input('items')['qty'][$key], 
            "price" => $request->input('items')['price'][$key],
          ]);
        }

        if($order->receipt_items)
        {
          foreach($order->receipt_items as $order_item)
          {
            $productItem = Product::where(['id'=>$order_item->product_id,])->first();
              $productItem->update([ 
                'qty'=>($order_item->qty + $productItem->qty), 
                'minimal_qty'=>0
              ]);
          }
        }
        return response()->json(['order'=>$order->receipt_items, 'status'=>'success']);
      }else{
        return response()->json(['order'=>null, 'status'=>'failed']);
      }
    }
  }
}
