<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptItem extends Model
{
    protected $fillable = ['receipt_id','product_id','qty','price'];

  public function receipt_order()
  {
    return $this->belongsTo('App\Receipt','receipt_id');
  }

  public function product()
  {
    return $this->belongsTo('App\Product','product_id');
  }
  
  public function setStock()
  {
    $stock = $this->qty;
    $this->product->qty += $stock;
    $this->product->save(); 
    return $stock;
  }
}
