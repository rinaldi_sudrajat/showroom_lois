$(document).ready(function() {
    $('#data-purchases').dataTable();
});

function validateNumber(input, evt) {
    var $this = $(input);
    if ((evt.which != 46 || $this.val().indexOf('.') != -1) &&
        ((evt.which < 48 || evt.which > 57) &&
            (evt.which != 0 && evt.which != 8))) {
        evt.preventDefault();
    }

    var text = $(input).val();
    if ((evt.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function() {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }

    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 2) &&
        (evt.which != 0 && evt.which != 8) &&
        ($(input)[0].selectionStart >= text.length - 2)) {
        evt.preventDefault();
    }
}

function removeData(input, evt) {
    subtotal = parseFloat($('.subtotal_' + $(input).attr("data-product")).text());
    total = parseFloat($('#transact_total').val());

    total = total - subtotal;

    $('#transact_total').val(total);
    summaryGrandTotal();
    $('tr#product_' + $(input).attr('data-product')).remove();

    evt.preventDefault();
}

function loadProducts() {
    var products = $('.product-list');
    var product = [];

    for (var i = 0; i < products.length; i++) {
        product.push($(products[i]).attr('data-id'));
    }

    if (product.length > 0) {
        product;
    } else {
        product = [0];
    }

    $.ajax({
        url: '/loadProducts',
        data: { product_id: product, filter: true },
        type: 'GET',
        success: function(data) {
            $('#list-productOrder').html(data.html.content);
            setTimeout(function() {
                $('#myModal').modal('show');
                $('.product-po').dataTable();
            }, 1);
        },
        error: function(data) {
            alert('data cannot be displayed');
        }
    })
    return product.length > 0 ? product : null;
}


function getCheckedBoxes(chkboxName) {
    var checkboxes = $('.' + chkboxName);
    var checkboxesChecked = [];

    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            checkboxesChecked.push($(checkboxes[i]).attr('data-id'));
        }
    }

    if (checkboxesChecked.length > 0) {
        $.ajax({
            url: '/addOrder',
            data: { product_id: checkboxesChecked },
            type: 'GET',
            success: function(data) {
                $('#orderItem').append(data.html.content)
                var totalDisc = 0;
                var totalTran = 0;

                $('.subtotal_detail').each(function() { totalTran += parseFloat($(this).text().replace(/,/g, '')) });
                $('#transact_total').val(parseFloat(totalTran).formatMoney(0, '.', ','));

                summaryGrandTotal();
                $('#myModal').modal('hide');
            },
            error: function(data) {
                console.log(data);
            }
        })
    }

    return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

function calcSubtotal(input, evt) {
    var $this = $(input);
    if ((evt.which != 46 || $this.val().indexOf('.') != -1) &&
        ((evt.which < 48 || evt.which > 57) &&
            (evt.which != 0 && evt.which != 8))) {
        evt.preventDefault();
    }

    var text = $(input).val();
    if ((evt.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function() {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }

    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 2) &&
        (evt.which != 0 && evt.which != 8) &&
        ($(input)[0].selectionStart >= text.length - 2)) {
        evt.preventDefault();
    }

    var totalDisc = 0;

    totalDisc = parseFloat($('.price_' + $(input).attr("data-product")).val().replace(/,/g, '')) * parseFloat($('.qty_' + $(input).attr("data-product")).val().replace(/,/g, ''));
    $('td.subtotal_' + $(input).attr("data-product")).html(parseFloat(totalDisc).formatMoney(0, '.', ','));

    var total = 0;

    $('.subtotal_detail').each(function() { total += parseFloat($(this).text().replace(/,/g, '')) });
    $('#transact_total').val(parseFloat(total).formatMoney(0, '.', ','));
    $('#grand_total').val(parseFloat(total).formatMoney(0, '.', ','));
    summaryGrandTotal();
}


function subtotalFunc(input, evt) {
    var totalDisc = 0;
    totalDisc = parseFloat($('.price_' + $(input).attr("data-product")).val()).formatMoney(0, '.', ',') * parseFloat($('.qty_' + $(input).attr("data-product")).val()).formatMoney(0, '.', ',');
    $('td.subtotal_' + $(input).attr("data-product")).html(totalDisc);
    $(input).remove()
    evt.preventDefault();
}

function calcDisc(input, evt) {
    var $this = $(input);
    if ((evt.which != 46 || $this.val().indexOf('.') != -1) &&
        ((evt.which < 48 || evt.which > 57) &&
            (evt.which != 0 && evt.which != 8))) {
        evt.preventDefault();
    }

    var text = $(input).val();
    if ((evt.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function() {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }

    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 2) &&
        (evt.which != 0 && evt.which != 8) &&
        ($(input)[0].selectionStart >= text.length - 2)) {
        evt.preventDefault();
    }

    var totalDisc = 0;
    var discVal = 0;
    var discValElem = $('.discVal');
    var totalTran = 0;

    grandtotal = parseFloat($("#grand_total").val().replace(/,/g, ''));
    subtotal = parseFloat($('.subtotal_' + $(input).attr("data-product")).text().replace(/,/g, ''));


    $('.subtotal_detail').each(function() { totalTran += parseFloat($(this).text().replace(/,/g, '')) });
    totalTran ? $('#transact_total').val(parseFloat(totalTran).formatMoney(0, '.', ',')) : $('#transact_total').val(0);
    transactTotal = parseFloat($('#transact_total').val().replace(/,/g, ''));
    grandTotal = (transactTotal)

    $('#grand_total').val(parseFloat(grandTotal).formatMoney(0, '.', ','));
}

function summaryGrandTotal() {
    disc = $('#supplier_discount').val() ? parseFloat($('#supplier_discount').val().replace(/,/g, '')) : 0;
    total = $('#transact_total').val() ? parseFloat($('#transact_total').val().replace(/,/g, '')) : 0;
    supplier_disc_item = $('#supplier_discount_item').val() ? parseFloat($('#supplier_discount_item').val().replace(/,/g, '')) : 0;
    taxCheck = $('#taxVal');
    service_value = parseFloat($('#service_value').val());

    if (service_value) {
        service_value = service_value;
    } else {
        service_value = 0;
    }

    if (disc) {
        disc = disc;
    } else {
        disc = 0;
    }

    if (supplier_disc_item) {
        supplier_disc_item = supplier_disc_item;
    } else {
        supplier_disc_item = 0;
    }
    calc = ((disc / 100) * total) + (supplier_disc_item / 100) * total;
    if (taxCheck.checked) {
        taxVal = total * 0.1;
    } else {
        taxVal = $('#taxVal').val() ? parseFloat($('#taxVal').val().replace(/,/g, '')) : 0;
    }

    $('#taxVal').val(parseFloat(taxVal).formatMoney(0, '.', ','));
    $('#discount_value').val(parseFloat(calc).formatMoney(0, '.', ','));

    transactTotal = $('#transact_total').val() ? parseFloat($('#transact_total').val().replace(/,/g, '')) : 0;
    grandTotal = transactTotal ? parseFloat((((transactTotal + taxVal) - parseFloat($('#discount_value').val().replace(/,/g, ''))) + service_value)).formatMoney(0, '.', ',') : 0;
    $('#grand_total').val(grandTotal ? parseFloat(grandTotal.replace(/,/g, '')).formatMoney(0, '.', ',') : 0);
}

function submitReceipts(frm, evt) {
    var products = $('.product-list');
    var product = [];
    var quantity = [];
    var price = [];

    po_number = $('#po_number').val();
    receipt_number = $('#receipt_number').val();
    receipt_date = $('#receipt_date').val();
    receipt_desc = $('#receipt_desc').val();

    for (var i = 0; i < products.length; i++) {
        product.push($(products[i]).attr('data-id'));
        quantity.push($($('.quantity')[i]).val());
        price.push(parseFloat($($('.price')[i]).val().replace(/,/g, '')));
    }

    $.ajax({
        url: $(frm).attr('action'),
        method: 'POST',
        data: {
            order: {
                po_number: po_number,
                receipt_number: receipt_number,
                receipt_date: receipt_date,
                receipt_desc: receipt_desc,
            },
            items: {
                product_id: product,
                qty: quantity,
                price: price,
            }
        },
        success: function(data) {
            if (data.status == 'success') {
                alert(data.status);
                window.location.href = "/receipts";
            } else {
                alert(data.status);
            }
        },
        error: function(data) {
            console.log(data);
        }
    });
    evt.preventDefault();
}

function calcTaxTrans(input) {
    var total = parseFloat($('#transact_total').val().replace(/,/g, ''));

    if (input.checked) {
        if (total > 0) {
            taxVal = total * 0.2;
            $('#taxTrans').val(parseFloat(taxVal).formatMoney(0, '.', ','));
            grand_total = (total-taxVal);
            $('#grand_total').val(parseFloat(grand_total).formatMoney(0, '.', ','));
        } else {
            $('#taxTrans').val(0);
        }
    } else {
        taxVal = parseFloat($('#taxTrans').val().replace(/,/g, ''));
        grand_total = parseFloat($('#grand_total').val().replace(/,/g, ''));
        $('#grand_total').val(parseFloat(grand_total + taxVal).formatMoney(0, '.', ','));
        $('#taxTrans').val(0);
    }
}

function calcTaxMember(input) {
    var total = parseFloat($('#transact_total').val().replace(/,/g, ''));

    if (input.checked) {
        if (total > 0) {
            taxVal = total * 0.1;
            $('#taxMember').val(parseFloat(taxVal).formatMoney(0, '.', ','));
            grand_total = (total - taxVal);
            $('#grand_total').val(parseFloat(grand_total).formatMoney(0, '.', ','));
        } else {
            $('#taxMember').val(0);
        }
    } else {
        taxVal = parseFloat($('#taxMember').val().replace(/,/g, ''));
        grand_total = parseFloat($('#grand_total').val().replace(/,/g, ''));
        $('#grand_total').val(parseFloat(grand_total+taxVal).formatMoney(0, '.', ','));
        $('#taxMember').val(0);
    }
}