$(document).ready(function(){
  $('#data-users').dataTable();
});

function setPayment(input, evt)
{
  if($(input).val()=="2")
  {
    $("#div_brand").show();
    $("#card_payment").val("");
  }else{
    $("#div_brand").hide();
    $("#card_payment").val($(input).val());
  }
  evt.preventDefault();
}


function destroyProduct(input){
  $.ajaxSetup({
    headers: { 'X-CSRF-Token': $('input[name="_token"]').val() }
  });
  var data = {id: $(input).attr('data')};
  $.ajax({
    url: "/products/"+$(input).attr('data'),
    type: 'DELETE',
    data : data,
    success: function(){
      $(input).parents('tr').remove();
      alert('Product was successfully disabled');
    },
    error: function(){
      alert('cannot delete this product');
    }
  });
}

+function ($) { "use strict";

  $(function(){
    var cache = {};

    $('#category_product').change(function(){
      $.getJSON('/product-category?cat='+$(this).val(),function(data){
        $('#category-spec').html(data.html.content);
      });
    });    

    var oTable = $('#data-products').dataTable( {
      "bProcessing": true,
      "sAjaxSource": "products",
      "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
      "sPaginationType": "full_numbers",
      "aoColumns": [
        { 
          "mData": function (data, type, full)
          {
            var urlView = "products/"+data.id;
            return "<a style='color: #428bca;' href='"+urlView+"'>"+data.name+"</a>";
          }    
        },
        { "mData": "qty" },
        { 
          "mData": function (data, type, full)
          {
            return parseFloat(data.sale_price).formatMoney(0, ',', '.');
          } 
        },
        { 
          "mData": function (data, type, full)
          {
            var urlView = "categories/"+data.category.id;
            return "<a style='color: #428bca;' href='"+urlView+"'>"+data.category.name+"</a>";
          } 
        },
        { 
          "mData": function (data, type, full)
          {
            var urlView = "/product-types?id="+data.type.id;
            if(data.type.name != '-')
            {
              return "<a style='color: #428bca;' href='"+urlView+"'>"+data.type.name+"</a>";
            }else{
              return data.type.name;
            }
          }  
        },
        { 
          "mData": function (data, type, full)
          {
            var urlView = "/product-size?id="+data.size.id;
            if(data.size.name != '-')
            {
              return "<a style='color: #428bca;' href='"+urlView+"'>"+data.size.name+"</a>";
            }else{
              return data.size.name;
            }
          }  
        },
        { 
          "mData": function (data, type, full)
          {
            var urlView = "/product-number?id="+data.number.id;
            if(data.number.name != '-')
            {
              return "<a style='color: #428bca;' href='"+urlView+"'>"+data.number.name+"</a>";
            }else{
              return data.number.name;
            }
          }  
        },
        { "mData" : function (data, type, full){
          var urlEdit = "products/"+data.id+"/edit";
          return "<a class='btn btn-info btn-sm' href='"+urlEdit+"'>Edit</a>  <a class='btn btn-info btn-sm' onclick='destroyProduct(this)' data="+data.id+" href='#'>Delete</a>";},
        }
      ]
    } );
  });
}(window.jQuery);

function receiptMonthly(input, evt){
  var data = {
    id: $("#product-mountly").attr('data-product'),
    from_date : $("#from_date").val(),
    to_date : $("#to_date").val()
   };
  $.ajax({
    url: "/report_by_po",
    type: 'GET',
    data : data,
    success: function(data){
      $('#product-mountly').html(data.html.content); 
    },
    error: function(){
      alert('Data Not Found !!');
    }
  });
  evt.preventDefault();
}

function sellMonthly(input, evt){
  var data = {
    id: $("#product-mountly-sell").attr('data-product'),
    from_date : $("#from_date").val(),
    to_date : $("#to_date").val()
   };
  $.ajax({
    url: "/report_by_sell",
    type: 'GET',
    data : data,
    success: function(data){
      $('#product-mountly-sell').html(data.html.content); 
    },
    error: function(){
      alert('Data Not Found !!');
    }
  });
  evt.preventDefault();
}