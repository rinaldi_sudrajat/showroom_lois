function destroyRoles(input){
  $.ajaxSetup({
    headers: { 'X-CSRF-Token': $('input[name="_token"]').val() }
  });
  
  var data = {id: $(input).attr('data')};

  $.ajax({
    url: "/roles/"+$(input).attr('data'),
    type: 'DELETE',
    data : data,
    success: function(){
      $(input).parents('tr').remove();
      alert('Role Success Delete');
    },
    error: function(){
      alert('cannot delete this Roles');
    }
  });
}

+function ($) { "use strict";

  $(function(){

  // datatable
    var oTable = $('#data-roles').dataTable( {
      "bProcessing": true,
      "sAjaxSource": "roles",
      "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
      "sPaginationType": "full_numbers",
      "aoColumns": [
        { 
          "mData": function (data, type, full)
          {
            var urlView = "roles/"+data.id;
            return "<a style='color: #428bca;' href='"+urlView+"'>"+data.name+"</a>";
          }    
        },
        { "mData": "display_name" },
        { "mData": "description"},
        {  "mData" : function (data, type, full){
          var url = "roles/edit/"+data.id;
          var urlView = "roles/"+data.id;
          return "<a class='btn btn-info btn-sm' href='"+url+"'>Edit</a> <a class='btn btn-info btn-sm' onclick='destroyRoles(this)' data="+data.id+" href='#'>Delete</a>";}    
        }
        ]
    });
});
}(window.jQuery);