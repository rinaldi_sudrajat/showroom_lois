function destroyCustomer(input){
  $.ajaxSetup({
    headers: { 'X-CSRF-Token': $('input[name="_token"]').val() }
  });
  
  var data = {id: $(input).attr('data')};

  $.ajax({
    url: "/customers/"+$(input).attr('data'),
    type: 'DELETE',
    data : data,
    success: function(){
      $(input).parents('tr').remove();
      alert('Costumer was successfully disabled');
    },
    error: function(){
      alert('cannot delete this Costumer');
    }
  });
}

function customerMonthly(input, evt){
  var data = {
    id: $("#customer-order").attr('data-customers'),
    from_date : $("#from_date").val(),
    to_date : $("#to_date").val()
   };

  $.ajax({
    url: "/report_by_customer",
    type: 'GET',
    data : data,
    success: function(data){
      $('#customer-order').html(data.html.content);
      
    },
    error: function(){
      alert('Data Not Found !!');
    }
  });
  evt.preventDefault();
}


+function ($) { "use strict";

  $(function(){

  // datatable
    var oTable = $('#data-customers').dataTable( {
      "bProcessing": true,
      "sAjaxSource": "customers",
      "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
      "sPaginationType": "full_numbers",
      "aoColumns": [
        { 
          "mData": function (data, type, full)
          {
            var urlView = "customers/"+data.id;
            return "<a style='color: #428bca;' href='"+urlView+"'>"+data.customer_number+"</a>";
          }    
        },
        { "mData": "full_name" },
        { "mData": "address" },
        { "mData": "phone"},
        { "mData": "email"},
        { "mData": "point_rewards"},
        {  "mData" : function (data, type, full){
          var url = "customers/edit/"+data.id;
          var urlView = "customers/"+data.id;
          return "<a class='btn btn-info btn-sm' href='"+url+"'>Edit</a> <a class='btn btn-info btn-sm' onclick='destroyCustomer(this)' data="+data.id+" href='#'>Delete</a>";}    
        }
        ]
    } );

    var oTableCat = $('#data-categories').dataTable( {
      "bProcessing": true,

      "sAjaxSource": "categories",
      "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
      "sPaginationType": "full_numbers",
      "aoColumns": [
        { "mData": "id" },
        { "mData": "name" },
        {  "mData" : function (data, type, full){
          var urlView = "categories/"+data.id;
          return "<a class='btn btn-info btn-sm' href='"+urlView+"'>View</a>";}    
        }
        ]
    } );
  });
}(window.jQuery);