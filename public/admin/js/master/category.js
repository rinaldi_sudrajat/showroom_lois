+function ($) { "use strict";
  $(function(){
  	 var oTableCat = $('#data-category').dataTable( {
      "bProcessing": true,
      "sAjaxSource": "categories",
      "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
      "sPaginationType": "full_numbers",
      "aoColumns": [
        { "mData": "id" },
        { "mData": "name" },
        {  "mData" : function (data, type, full){
          var urlView = "categories/"+data.id;
          return "<a class='btn btn-info btn-sm' href='"+urlView+"'>View</a>";}    
        }
        ]
    } );
  });
}(window.jQuery);