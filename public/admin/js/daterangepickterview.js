$('#dob').daterangepicker({
      locale: {
      format: 'YYYY-MM-DD'
      },
  minDate: '1970-01-01',
  maxDate: '1999-01-01',
  singleDatePicker: true,
  showDropdowns: true,
  calender_style: "picker_2"
}, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
});