$(document).ready(function() {
    $('#data-sales').dataTable();
    $('#data-report-sales').dataTable();
});

function submitSalesOrder(frm, evt) {
    var products = $('.product-list');
    var product = [];
    var quantity = [];
    var price = [];

    sales_number = $('#sales_number').val();
    sales_date = $('#from_date').val();
    customer_id = $('#customer_id').val();
    customer_value = parseFloat($('#taxMember').val().replace(/,/g, ''));
    discountvalue = parseFloat($('#taxTrans').val().replace(/,/g, ''));
    description = $('#description').val();
    payment = $('#payment_id').val();
    edc_payment = $('#edc_payment_id').val();
    for (var i = 0; i < products.length; i++) {
        product.push($(products[i]).attr('data-id'));
        quantity.push($($('.quantity')[i]).val());
        price.push(parseFloat($($('.price')[i]).val().replace(/,/g, '')));
    }

    $.ajax({
        url: $(frm).attr('action'),
        method: 'POST',
        data: {
            order: {
                sales_number: sales_number,
                sales_date: sales_date,
                customer_id: customer_id,
                customer_value: customer_value,
                discount_value: discountvalue,
                description: description,
                payment_id: payment,
                edc_payment_id: edc_payment,        
            },
            items: {
                product_id: product,
                qty: quantity,
                price: price
            }
        },
        beforeSend: function() {
            $('#loading').modal();
        },
        complete: function() {
            $('#loading').modal('hide');
        },
        success: function(data) {
            if (data.status == 'success') {
                alert(data.status);
                window.location.href = "/sales_orders";
            } else {
                // alert(data.status);
            }
        },
        error: function(data) {
            // console.log(data.status);
        }
    });
    evt.preventDefault();
}

function findCustomer(evt) {
    phone_number = $('#phone_number').val();
    $.ajax({
        url: '/findCustomer',
        data: { phone_number: phone_number },
        type: 'GET',
        success: function(data) {
            if (data.customer.length > 0) {
                $('#customer_name').val(data.customer[0].full_name);
                $('#customer_address').val(data.customer[0].address);
                $('#phone_number').attr('data-customer', data.customer[0].id);
            } else {
                $('#customer-form').html(data.html.content);
                setTimeout(function() {
                    $('#date').daterangepicker({
                        locale: {
                            format: 'YYYY-MM-DD'
                        },
                        minDate: '1930-01-01',
                        singleDatePicker: true,
                        showDropdowns: true,
                        calender_style: "picker_2"
                    });
                    $('#customerModal').modal('show');
                }, 1);
            }
        },
        error: function(data) {
            alert('data cannot be displayed');
        }
    });
    evt.preventDefault();
}

function loadProductSales() {
    var products = $('.product-list');
    var product = [];

    for (var i = 0; i < products.length; i++) {
        product.push($(products[i]).attr('data-id'));
    }

    if (product.length > 0) {
        product;
    } else {
        product = [0];
    }

    $.ajax({
        url: '/loadProductSales',
        data: { product_id: product },
        type: 'GET',
        success: function(data) {
            if (data) {
                $('#list-productOrder').html(data.html.content);
                setTimeout(function() {
                    $('#myModal').modal('show');
                    $('.product-po').dataTable();
                }, 1);
            } else {
                alert('data not found');
            }
        },
        error: function(data) {
            alert('data cannot be displayed');
        }
    })

    return product.length > 0 ? product : null;
}

function getCheckedBoxesSales(chkboxName) {
    var checkboxes = $('.' + chkboxName);
    var checkboxesChecked = [];

    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            checkboxesChecked.push($(checkboxes[i]).attr('data-id'));
        }
    }

    if (checkboxesChecked.length > 0) {
        $.ajax({
            url: '/addSalesOrder',
            data: { product_id: checkboxesChecked },
            type: 'GET',
            beforeSend: function() {
                $('#loading').modal();
            },
            complete: function() {
                $('#loading').modal('hide');
            },
            success: function(data) {
                $('#orderItem').append(data.html.content)
                var totalDisc = 0;
                var totalTran = 0;

                $('.subtotal_detail').each(function() { totalTran += parseFloat($(this).text().replace(/,/g, '')) });
                $('#transact_total').val(parseFloat(totalTran).formatMoney(0, '.', ','));

                summaryGrandTotal();

                $('#myModal').modal('hide');
            },
            error: function(data) {
                // console.log(data);
            }
        })
    }

    return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

function checkStock(input, evt){
  var qty = $('.qty_'+$(input).attr("data-product")).val();
  var min_qty = $('.minqty'+$(input).attr("data-product")).val();

  stockTotal = min_qty - qty;

  if(stockTotal < 0){
    swal({
      title: "Warning!",
      text: "Product Out Of Stock",
      imageUrl: "/admin/images/sold-out.png"
    });
    document.getElementById("button").disabled = true
  }else if(stockTotal == 0){
    document.getElementById("button").disabled = false
  }
  else{
    document.getElementById("button").disabled = false
  }
}