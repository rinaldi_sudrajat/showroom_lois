<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_category_id');
            $table->integer('product_type_id');
            $table->integer('product_size_id');
            $table->string('barcode',100);
            $table->string('name',30);
            $table->decimal('price_list', 9, 2)->default(0);
            $table->decimal('sale_price', 9, 2)->default(0);
            $table->decimal('sale_price_discount',5,2)->default(0);
            $table->integer('minimum_qty')->default(0);
            $table->integer('qty')->default(0);
            $table->string('picture');
            $table->timestamps();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
