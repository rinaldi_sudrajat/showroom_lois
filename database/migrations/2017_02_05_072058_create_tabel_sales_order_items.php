<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelSalesOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('sales_order_items', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('sales_order_id');
      $table->integer('product_id');
      $table->integer('qty')->default(0);
      $table->decimal('price',9,2)->default(0);
      $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::drop('sales_order_items');
        
    }
}
