<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelSalesOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sales_number')->index()->unique();
            $table->date('sales_date')->index();
            $table->integer('customer_id')->index();
            $table->decimal('customer_value',9,2)->default(0);
            $table->decimal('discount_value',9,2)->default(0);
            $table->integer('user_id');
            $table->integer('payment_id')->default(0);
            $table->text('description');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_orders');
    }
}
