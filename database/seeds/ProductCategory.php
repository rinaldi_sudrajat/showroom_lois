<?php

use Illuminate\Database\Seeder;
class ProductCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->insert(
        [
            [
                'name' => 'Celana'
            ],
            [
                'name' => 'Jaket'
            ],
            [
                'name' => 'T-Shirt'
            ],
            [
                'name' => 'Kemeja'
            ],
        ]);
    }
}
