<?php

use Illuminate\Database\Seeder;

class Product extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	'product_type_id' => 2,
        	'product_size_id' => 2,
        	'product_size_number_id' => 2,
        	'product_category_id' => 2,
        	'name' => '91238',
        	'price_list' => 1500,
        	'sale_price' => 2000,
        	'qty' => 0,
        	'picture' => 'default.jpg',
            'active' => true, 
       	]);
    }
}
