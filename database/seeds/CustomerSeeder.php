<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
      DB::table('customers')->truncate();

      $faker = Faker\Factory::create();

       for($i = 0; $i < 500; $i++) {
        App\Customer::create([
            'customer_number' => "COS-".mt_rand(),
            'full_name' => $faker->name,
            'dob' => $faker->dateTime,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'point_rewards' => 0,
            'active' => true,
        ]);
    }
    }
}
