<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert(
        [
            [
                'name' => 'Administrator',
                'password' => bcrypt('administrator_password'),
                'email' => 'administrator@lois.com'
            ],
            [
                'name' => 'Store',
                'password' => bcrypt('store_password'),
                'email' => 'store@lois.com',
            ]
        ]);
    }
}
