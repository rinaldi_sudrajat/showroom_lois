<?php

use Illuminate\Database\Seeder;
class ProductType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_types')->insert(
        [
            [
                'name' => 'Bottom'
            ],
            [
                'name' => 'Up'
            ],
        ]);
    }
}
