<?php

use Illuminate\Database\Seeder;
use App\Roles;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	[
        		'name' => 'admin',
        		'display_name' => 'Administrator',
        		'description' => '-'
        	],

            [
        		'name' => 'kasir',
        		'display_name' => 'Kasir',
        		'description' => '-'
        	],

            [
        		'name' => 'wherehouse',
        		'display_name' => 'Wherehouse',
        		'description' => '-'
        	],

            [
        		'name' => 'manager',
        		'display_name' => 'Manager',
        		'description' => '-'
        	],
        ];

        foreach ($roles as $key => $value) {
        	Roles::create($value);
        }
    }
}
