<?php

use Illuminate\Database\Seeder;
use App\Permission;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
        	[
        		'name' => 'management-setup-user',
        		'display_name' => 'Management Setup User',
        		'description' => '-'
        	],

        	[
        		'name' => 'management-setup-role',
        		'display_name' => 'Management Setup Role',
        		'description' => '-'
        	],

        	[
        		'name' => 'master-data-product-type',
        		'display_name' => 'Master Product Type',
        		'description' => '-'
        	],
            
            [
                'name' => 'master-data-product-category',
                'display_name' => 'Master Product Category',
                'description' => '-'
            ],      	
            [
        		'name' => 'master-data-product',
        		'display_name' => 'Master Product',
        		'description' => '-'
        	],
        	[
        		'name' => 'master-data-customer',
        		'display_name' => 'Master Data Customers',
        		'description' => '-'
        	],
        	[
        		'name' => 'transcation-good-receipt',
        		'display_name' => 'Transaksi Good Receipt',
        		'description' => '-'
        	],
        	[
        		'name' => 'transaction-sale-orders',
        		'display_name' => 'Transaksi Sales Orders',
        		'description' => '-'
        	]
        ];

        foreach ($permission as $key => $value) {
        	Permission::create($value);
        }
    }
    
}
