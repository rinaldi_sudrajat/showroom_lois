<?php

use Illuminate\Database\Seeder;
class ProductSize extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_sizes')->insert(
        [
            [
                'name' => '-'
            ],
            [
                'name' => 'X'
            ],
            [
                'name' => 'M'
            ],
            [
                'name' => 'XL'
            ],
            [
                'name' => 'S'
            ],
            [
                'name' => 'L'
            ],
        ]);
    }
}
