<?php

use Illuminate\Database\Seeder;

class PaymentTableSeederPlus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('edc_payments')->insert([
          [
          	'name' => 'EDC BNI KREDIT'
          ],
          [
          	'name' => 'EDC BNI DEBIT'
          ],
          [
          	'name' => 'EDC BCA DEBIT'
          ],
          [
          	'name' => 'EDC BCA KREDIT'
          ],
        ]);
    }
}
