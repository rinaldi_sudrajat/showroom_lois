<?php

use Illuminate\Database\Seeder;
class ProductSizeNumber extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_size_numbers')->insert(
        [
            [
                'name' => '-'
            ],
            [
                'name' => '20'
            ],
            [
                'name' => '25'
            ],
            [
                'name' => '30'
            ],
            [
                'name' => '31'
            ],
            [
                'name' => '32'
            ],
            [
                'name' => '33'
            ],
        ]);
    }
}
