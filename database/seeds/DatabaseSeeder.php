<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProductCategory::class);
        $this->call(ProductSize::class);
        $this->call(ProductSizeNumber::class);
        $this->call(ProductType::class);
        $this->call(Product::class);
        $this->call(CustomerSeeder::class);
        $this->call(PaymentSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(RolePermissionUserSeeder::class);
    }
}
