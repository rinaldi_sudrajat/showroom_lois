<?php

use Illuminate\Database\Seeder;
class RolePermissionUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolesAdmin = DB::table('roles')->insert([
        	'name' => 'Admin',
        	'display_name' => 'Administrator Toko',
        	'description' => '-'
        ]);

        $rolesKasir = DB::table('roles')->insert([
        	'name' => 'Kasir',
        	'display_name' => 'Kasir Toko',
        	'description' => '-'
        ]);

        $permission = DB::table('permission_role')->insert([
        [
        	'permission_id' => 1,
        	'role_id' => 1
        ],

        [
        	'permission_id' => 2,
        	'role_id' => 1
        ],

        [
        	'permission_id' => 3,
        	'role_id' => 1
        ],

        [
        	'permission_id' => 4,
        	'role_id' => 1
        ],
        [
        	'permission_id' => 5,
        	'role_id' => 1
        ],
        [
        	'permission_id' => 6,
        	'role_id' => 1
        ],
        [
        	'permission_id' => 7,
        	'role_id' => 1
        ],
        [
        	'permission_id' => 8,
        	'role_id' => 1
        ]
        ]);

        $roleUser = DB::table('role_user')->insert([
        	'user_id' => 1,
        	'role_id' => 1
        ]);
    }
}
