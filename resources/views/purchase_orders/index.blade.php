@extends('layouts.default')
@section('content')
  <section class="scrollable padder">
    <section class="panel panel-default">
      <header class="panel-heading">
        Data Purchase Orders
        <a href="{{ URL('purchase_orders/create') }}" class="btn btn-primary pull-right btn-sm">Add Order</a><br>
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
      </header>
      <div class="table-responsive">
        <table id="data-purchases" class="table table-striped m-b-none" data-ride="datatables">
          <thead>
            <tr>
              <th>PO number</th>
              <th>Date</th>
              <th>Order by</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($purchase_orders as $purchase)
            <tr>
              <td>{{$purchase->order_number}}</td>
              <td>{{$purchase->order_date}}</td>
              <td>{{$purchase->user->name}}</td>
              <td><a class='btn btn-info btn-sm' href="{{ URL('purchase_orders', $purchase->id) }}">View</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
  </section>
@endsection