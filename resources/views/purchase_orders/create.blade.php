@extends('layouts.default')
@section('content')
<div class="row">
  <div class="col-sm-12">
    {!! Form::open(['url' => 'purchase_orders', 'class' => 'form-horizontal',"data-validate"=>"parsley","id"=>"po_create", "onsubmit"=>"submitPO(this, event)"]) !!}
      <section class="panel panel-default">
        <header class="panel-heading">
          <strong>Purchase Orders</strong>
        </header>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-3 control-label">Date</label>
            <div class="col-sm-9">
              {{Form::date('po_date', \Carbon\Carbon::now(), ['class'=>'form-control','data-notblank'=>"true", 'readonly'=>true, "id"=>"po_date"])}}
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 control-label">Description</label>
            <div class="col-sm-9">
              {{Form::textarea('description', null, ['rows'=>2, 'class'=>'form-control', 'id' => 'description'])}}
            </div>
          </div>
        
          <div class="form-group">
            <label class="col-sm-3 control-label">Order By</label>
            <div class="col-sm-9">
              {{Form::text('user',Auth::user()->name,['class' => 'form-control','placeholder'=>'Order By','required'=>true, 'disabled'=>true])}}
            </div>
          </div>

          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <div class="col-sm-12 table-responsive">
              <table id="data-orders" class="table table-striped m-b-none" data-ride="datatables">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Category</th>
                    <th>Size</th>
                    <th>Number</th>
                    <th>Qty</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="orderItem">
                </tbody>
                <tr>
                  <td colspan="9">
                  <a href="#" class="btn btn-success" onclick="loadProducts();">Add product</a></td>
                </tr>
              </table>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
        <footer class="panel-footer text-right bg-light lter">
          <button type="submit" class="btn btn-success btn-s-xs">Submit</button>
        </footer>
      </section>
    {{Form::close()}}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Product</h4>
          </div>
          <div class="modal-body" id="list-productOrder">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="getCheckedBoxes('checkboxName')">Add</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection