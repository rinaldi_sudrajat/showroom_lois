@extends('layouts.default')
@section('content')
@foreach($products as $product)
  <tr id="product_{{$product->id}}" class="product-list" data-id="{{$product->id}}">
    <td>{{$product->name}}</td>
    <td>{{$product->category->name}}</td>
    <td>{{$product->size->name}}</td>
    <td>{{$product->number->name}}</td>
    <td><input type="text" data-subtotal="subtotal_{{$product->id}}" data-product="{{$product->id}}" name="quantity" value="1" onkeypress="validateNumber(this, event)" onkeyup="calcSubtotal(this, event)" class="payments qty_{{$product->id}} quantity" value="1"></td>
    <td>
      <a href="#" class="btn btn-primary" onclick="removeData(this, event)" data-product="{{$product->id}}">X</a>
    </td>
  </tr>
  @endforeach
@endsection
