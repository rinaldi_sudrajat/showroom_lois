<meta charset="utf-8" />
<title>Showroom Lois | Web Application</title>
<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
<link rel="stylesheet" href="{{URL('admin/css/bootstrap.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('admin/js/daterangepicker/daterangepicker.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('admin/css/animate.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('admin/css/font-awesome.min.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('admin/css/icon.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('admin/css/font.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('admin/js/calendar/bootstrap_calendar.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('admin/css/landing.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('admin/css/app.css')}}" type="text/css" />  
<link rel="stylesheet" href="{{URL('admin/js/datatables/datatables.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{URL('admin/css/jquery-ui.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{URL('admin/css/sweetalert.css')}}" type="text/css">
<link rel="stylesheet" href="{{URL('admin/css/chosen.min.css')}}" type="text/css"/>
@include('layouts._script.bottom')
<!--[if lt IE 9]>
  <script src="js/ie/html5shiv.js"></script>
  <script src="js/ie/respond.min.js"></script>
  <script src="js/ie/excanvas.js"></script>
<![endif]-->