<script>
Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
 </script>
<script src="{{URL('admin/js/jquery.js')}}"></script>
<script src="{{URL('admin/js/jquery-ui.js')}}"></script>
 <script src="{{URL('admin/js/daterangepicker/moment.min.js')}}"></script>
  <script src="{{URL('admin/js/chosen.jquery.min.js')}}"></script>
  <!-- Bootstrap -->
  <script src="{{URL('admin/js/bootstrap.js')}}"></script>
  <!-- App -->
  <script src="{{URL('admin/js/app.js')}}"></script>
  <script src="{{URL('admin/js/sweetalert.min.js')}}"></script>
  {{-- Date Range Picker --}}
  <script src="{{URL('admin/js/daterangepicker/daterangepicker.js')}}"></script>
  <script src="{{URL('admin/js/daterangepickterview.js')}}"></script>
  <script src="{{URL('admin/js/slimscroll/jquery.slimscroll.min.js')}}"></script>
  <script src="{{URL('admin/js/charts/easypiechart/jquery.easy-pie-chart.js')}}"></script>
  <script src="{{URL('admin/js/charts/sparkline/jquery.sparkline.min.js')}}"></script>
  <script src="{{URL('admin/js/charts/flot/jquery.flot.min.js')}}"></script>
  <script src="{{URL('admin/js/charts/flot/jquery.flot.tooltip.min.js')}}"></script>
  <script src="{{URL('admin/js/charts/flot/jquery.flot.spline.js')}}"></script>
  <script src="{{URL('admin/js/charts/flot/jquery.flot.pie.min.js')}}"></script>
  <script src="{{URL('admin/js/charts/flot/jquery.flot.resize.js')}}"></script>
  <script src="{{URL('admin/js/charts/flot/jquery.flot.grow.js')}}"></script>
  <script src="{{URL('admin/js/charts/flot/demo.js')}}"></script>
  <script src="{{URL('admin/js/calendar/bootstrap_calendar.js')}}"></script>
  <script src="{{URL('admin/js/calendar/demo.js')}}"></script>
  <script src="{{URL('admin/js/sortable/jquery.sortable.js')}}"></script>
  <script src="{{URL('admin/js/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{URL('admin/js/app.plugin.js')}}"></script>
  <script src="{{URL('admin/js/master/category.js')}}"></script>
  <script src="{{URL('admin/js/master/type.js')}}"></script>
  <script src="{{URL('admin/js/master/product.js')}}"></script>
  <script src="{{URL('admin/js/master/customer.js')}}"></script>
  <script src="{{URL('admin/js/purchase_order.js')}}"></script>
  <script src="{{URL('admin/js/sales_order.js')}}"></script>
  <script src="{{URL('admin/js/master/roles.js')}}"></script>
    <script src="{{URL('admin/js/report_so.js')}}"></script>

