<!DOCTYPE html>
<html lang="en" class="app">
	<head>
		@include('layouts._script.up')
	</head>
	<body class="" >
		<section class="vbox">
			@include('layouts.main.header')
			<section>
				<section class="hbox stretch">
					@include('layouts.main.navigation')
					<section id="content">
						<section class="hbox stretch">
							<section class="vbox">
								<section class="scrollable padder">
									
									@yield('content')
								</section>
							</section>
						</section>
						<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
					</section>
				</section>
				@include('layouts.main.footer')
			</section>
		</section>
	</body>
</html>