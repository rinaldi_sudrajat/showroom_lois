<aside class="bg-black aside-md hidden-print hidden-xs" id="nav">          
  <section class="vbox">
    <section class="w-f scrollable">
      <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
        <div class="clearfix wrapper dk nav-user hidden-xs">
          <div class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="thumb avatar pull-left m-r">                        
                <img src="{{URL('admin/images/lois.jpg')}}" class="dker" alt="...">
                <i class="on md b-black"></i>
              </span>
              <span class="hidden-nav-xs clear">
                <span class="block m-t-xs">
                  <strong class="font-bold text-lt">{{ Auth::guest() ? "Anonymous" : Auth::user()->name }}</strong> 
                  <b class="caret"></b>
                </span>
                <span class="text-muted text-xs block">{{ Auth::guest() ? "Anonymous" : Auth::user()->name }}</span>
              </span>
            </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
              <li>
                <a href="{{URL('/logout')}}">Logout</a>
              </li>
            </ul>
          </div>
        </div>
        <!-- nav -->                 
        <nav class="nav-primary hidden-xs">
          <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">Start</div>
          <ul class="nav nav-main" data-ride="collapse">
            <li  class="active">
              <a href="/" class="auto">
                <i class="i i-statistics icon"></i>
                <span class="font-bold">Dashboard</span>
              </a>
            </li>
            <li >
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="i i-circle-sm-o text"></i>
                  <i class="i i-circle-sm text-active"></i>
                </span>
                <i class="i i-stack icon">
                </i>
                <span class="font-bold">Management</span>
              </a>
              <ul class="nav dk">
              @permission('management-setup-user')
              <li>
                  <a href="{{URL('users')}}" class="auto">                               
                    <i class="i i-dot"></i>
                    <span>User</span>
                  </a>
              </li>
              @endpermission
               @permission('management-setup-role') 
                <li >
                  <a href="{{URL('roles')}}" class="auto">                                                        
                    <i class="i i-dot"></i>

                    <span>Access Role</span>
                  </a>
                </li>
              @endpermission
              </ul>
            </li>
            <li >
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="i i-circle-sm-o text"></i>
                  <i class="i i-circle-sm text-active"></i>
                </span>
                <i class="i i-stack icon">
                </i>
                <span class="font-bold">Master Data</span>
              </a>
              <ul class="nav">
              @permission('master-data-product-category')
                <li >
                  <a href="{{URL('categories')}}">                                                  
                    <i class="i i-dot"></i>
                    <span>Product Category</span>
                  </a>
                </li>
                @endpermission
                @permission('master-data-product-type')
                <li >
                  <a href="{{URL('types')}}">            
                    <i class="i i-dot"></i>
                    <span>Product Type</span>
                  </a>
                </li>
                @endpermission
                @permission('master-data-product')
                <li >
                  <a href="{{URL('products')}}">                                                  
                    <i class="i i-dot"></i>
                    <span>Product</span>
                  </a>
                </li>
                @endpermission
                @permission('master-data-customer')
                <li >
                  <a href="{{URL('customers')}}">                                       
                    <i class="i i-dot"></i>
                    <span>Customers</span>
                  </a>
                </li>
                @endpermission
              </ul>
            </li>
            <li>
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="i i-circle-sm-o text"></i>
                  <i class="i i-circle-sm text-active"></i>
                </span>
                <i class="i i-stack icon">
                </i>
                <span class="font-bold">Transactions</span>
              </a>
              <ul class="nav">
                @permission('transcation-good-receipt')
                <li >
                  <a href="{{URL('receipts')}}">                                                  
                    <i class="i i-dot"></i>
                    <span>Good Receipts</span>
                  </a>
                </li>
                @endpermission
                @permission('transaction-sale-orders')
                <li >
                  <a href="{{URL('sales_orders')}}">                                                  
                    <i class="i i-dot"></i>
                    <span>Sales Orders</span>
                  </a>
                </li>
                @endpermission
              </ul>
            </li>

              <li>
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="i i-circle-sm-o text"></i>
                  <i class="i i-circle-sm text-active"></i>
                </span>
                <i class="i i-stack icon">
                </i>
                <span class="font-bold">Reporting</span>
              </a>
              <ul class="nav">
                <li >
                  <a href="{{URL('report_so')}}">                                                  
                    <i class="i i-dot"></i>
                    <span>Sales order Daily</span>
                  </a>
                </li>
                  <li >
                  <a href="{{URL('report_mounth')}}">                                                  
                    <i class="i i-dot"></i>
                    <span>Sales order Mounth</span>
                  </a>
                </li>
                <li >
                  <a href="{{URL('report_stock')}}">                                                  
                    <i class="i i-dot"></i>
                    <span>Reporting Product</span>
                  </a>
                </li>
              </ul>
            </li>

          </ul>
          <div class="line dk hidden-nav-xs"></div>
        </nav>
        <!-- / nav -->
      </div>
    </section>
  </section>
</aside>
<!-- /.aside -->