@extends('layouts.default')
@section('content')
<section class="scrollable padder">
  <section class="panel panel-default">
    <header class="panel-heading">
      Data Product Category
    </header>
    <div class="table-responsive">
    <table id="data-productTypes" class="table table-striped m-b-none" data-ride="datatables">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    </div>
  </section>
</section>
@endsection