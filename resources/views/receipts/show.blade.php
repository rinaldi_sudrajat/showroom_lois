<style>
@media print{
          body {margin-top: 0.08mm; margin-bottom: 2.79mm; 
           margin-left: 0.15mm; margin-right: 0.15mm;}
          font-fam: Times New Roman;
          .hidden-print,
          .page-bar,
          .page-footer,
          .page-quick-sidebar-wrapper,
          .page-sidebar-wrapper{
          display: none;
}
    @page {
                /*size: A portrait;*/
                size: 9.5in 11in !important;
                font-size: 13px !important;
                /*font-family: Arial;*/
            }

    @font-face {font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;}
}
</style>
<body onload="window.print()">
<div class="clearfix"></div>

<table align="center" border=0 width="97%" style="margin-top:5px;  border:1px solid black;">
    <tr>
    <td colspan="3" align="center" style="padding: 5px; font-weight: bold; font-size: 16px; text-decoration: underline;">PENERIMAN BARANG</td>
  </tr>
  <tr>
    <td colspan="3" align="center" style="font-weight: bold;font-size: 13px;border-bottom: 1px solid black;">PO-NUMBER &nbsp;{{$data->po_number}}</td>
  </tr>
    <tr>
    <td width="15%" style="padding: 2px;font-weight: bold;font-size: 12px;">PO Date</Td>
    <td style="font-size: 15px;">:&nbsp;{{$data->receipt_date}}</Td>
  </tr>
  <tr>
    <td width="15%" style="padding: 2px; font-weight: bold; font-size: 12px;">Desc</Td>
    <td style="font-size: 15px;">:&nbsp;{{$data->receipt_desc}}</Td>
  </tr>
</table>


<table align="center" border=1 width="97%" style="margin-top:5px;  border:1px solid black;">
  <tr>
    <td align="center">Product Name</td>
    <td align="center">Quantity</td>
    <td align="center">Price</td>
    <td align="center">Subtotal</td>
  </tr>
  @foreach($data->receipt_items as $item)
  <tr>
    <td align="left">{{empty($item->product->name) ? 'Product tidak ada' : $item->product->name}}</td>
    <td align="right">{{$item->qty}}</td>
    <td align="right">{{number_format($item->price)}}</td>
    <td align="right">{{number_format($item->qty * $item->price)}}</td>
  </tr>
  @endforeach
</table>
<table align="center" border=0 width="97%" style="margin-top:5px;  border:1px solid black;">
   <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right"></td>
      <td width="10%"></td>
   </tr>
   <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right"></td>
      <td width="10%"></td>
   </tr>

   <tr>
    <td align="center">{{$data->user->name}}</td>
    <td></td>
    <td></td>
    <td></td>
   </tr>
</table>