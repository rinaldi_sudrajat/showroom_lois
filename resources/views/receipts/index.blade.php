@extends('layouts.default')
@section('content')
  <section class="scrollable padder">
    <section class="panel panel-default">
      <header class="panel-heading">
        Good Receipts
        <a href="{{ URL('receipts/create') }}" class="btn btn-primary pull-right btn-sm">Add Receipt</a><br>
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
      </header>
      <div class="table-responsive">
        <table id="data-purchases" class="table table-striped m-b-none" data-ride="datatables">
          <thead>
            <tr>
              <th>Received number</th>
              <th>Date</th>
              <th>Order by</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($receipts as $items)
            <tr>
              <td>{{$items->receipt_number}}</td>
              <td>{{$items->receipt_date}}</td>
              <td>{{$items->user->name}}</td>
              <td><a class='btn btn-info btn-sm' href="{{ URL('receipts', $items->id) }}">View</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
  </section>
@endsection