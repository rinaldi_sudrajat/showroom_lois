@extends('layouts.default')
@section('content')
<section class="panel panel-default">
  <header class="panel-heading font-bold">Insert Product Master</header>
  <div class="panel-body">
    {!! Form::open(['url' => 'products', 'class' => 'form-horizontal','files' => true]) !!}
      <div class="form-group">
        <label class="col-sm-2 control-label">Category</label>
        <div class="col-sm-4">
          {{Form::select('product_category_id',$category,null,['id'=>'category_product','class' => 'form-control chosen-select','placeholder'=>'Select Category','required'=>true])}}
        </div>
      </div>
      <div id='category-spec'>
        <!-- render data -->
      </div>
      
      <div class="line line-dashed b-b line-lg pull-in"></div>

      <div class="form-group">
        <label class="col-sm-2 control-label" for="input-id-1">Product Name</label>
        <div class="col-sm-6">
          {{Form::text('name',null,['class' => 'form-control','id'=>'name','required'=>true,'onkeydown'=>'getNameProduct()','placeholrder'=>'Tab for get name product'])}}
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label" for="input-id-1">Sale Price</label>
        <div class="col-sm-2">
          {{Form::text('sale_price',null,['class' => 'form-control','id'=>'sale_price','required'=>true,'onkeypress'=>'return isNumber(event)'])}}
        </div>
      </div>

      <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Minimun Qty</label>
          <div class="col-sm-2">
              {{Form::number('minimum_qty',null,['class' => 'form-control','id'=>'minimum_qty','required'=>true])}}
          </div>
      </div>

      <div class="form-group">
        <img src="" style="display:none;" alt="product-image" class="image-products img-responsive"/>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Get Picture</label>
        <div class="col-sm-6">
        <!-- image-preview-filename input [CUT FROM HERE]-->
            <div class="input-group image-preview">
                <input type="text" name="picture" onchange="readURL(this)" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" name="picture" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                    </div>
                </span>
            </div><!-- /input-group image-preview [TO HERE]--> 
          {{-- {{Form::file('picture',['class' => 'filestyle','data-icon' => 'false','data-classButton' => 'btn btn-default' ,'data-classInput' => 'form-control inline v-middle input-s','onchange'=>"readURL(this)", 'data-image'=>".image-products"])}} --}}
        </div>
      </div>      
      <div class="line line-dashed b-b line-lg pull-in"></div>
      
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-4">
          {!! Form::submit('Create', ['class' => 'btn btn-primary form-control','id' => 'submit']) !!}
        </div>
      </div>
    {{Form::close()}}
  </div>
</section>	
@endsection