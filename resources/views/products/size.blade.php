@extends('layouts.default')
@section('content')
<section class="vbox">
  <section class="scrollable padder">
    <div class="m-b-md">
      <h3 class="m-b-none">
        <a href="{{ URL('products') }}" class="btn btn-primary"><i class="i i-circleleft text"></i>&nbsp; Back</a>
      </h3>
      <br>
      <header class="panel-heading">
      Data Product by Size <b>{{$size->name}}</b>
    </header>
    <section class="panel panel-default">
      <div class="table-responsive">
        <table class="table table-striped b-t b-light">
          <thead>
            <tr>
              <th>Name</th>
              <th>Category</th>
              <th>qty</th>
            </tr>
          </thead>
          <tbody>
            @foreach($size->product as $product)
              <tr>
                <td><a href="{{route('products.show', $product->id)}}">{{$product->name}}</a></td>
                <td>{{$product->category->name}}</td>
                <td>{{$product->qty}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
</section>
</section>
@endsection