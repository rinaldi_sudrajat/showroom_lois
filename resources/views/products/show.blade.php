@extends('layouts.default')
@section('content')
<section class="vbox">
  <section class="scrollable padder">
    <div class="m-b-md">
      <h3 class="m-b-none">
        <a href="{{URL('products')}}" class="btn btn-primary"><i class="i i-circleleft text"></i>&nbsp; Back</a>
      </h3>
      <br>
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Product Detail</a></li>
        <li><a data-toggle="tab" href="#menu3">History Good Receipt</a></li>
        <li><a data-toggle="tab" href="#menu4">History Sell Out</a></li>
      </ul>
      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-sm-5">
              <section class="panel panel-default">
                <header class="panel-heading">
                  <strong>Product Details</strong>
                </header>

                <table class="table table-striped m-b-none">
                  <tbody>
                    <tr>   
                      <td>Category</td>
                      <td>:</td>
                      <td>{{$product->category->name}}</td>
                    </tr>
                    <tr>   
                      <td>Name</td>
                      <td>:</td>
                      <td>{{$product->name}}</td>
                    </tr>
                    <tr>   
                      <td>Size</td>
                      <td>:</td>
                      <td>{{$product->merk ? $product->merk->name : '-'}}</td>
                    </tr>
                    <tr>   
                      <td>Type</td>
                      <td>:</td>
                      <td>{{$product->type ? $product->type->name : '-'}}</td>
                    </tr>
                    <tr>   
                      <td>Size Number</td>
                      <td>:</td>
                      <td>{{$product->width ? $product->width->name : '-'}}</td>
                    </tr>
                    <tr>   
                      <td>Sale Price</td>
                      <td>:</td>
                      <td>{{number_format($product->sale_price)}}</td>
                    </tr>
                    <tr>
                      <td>Minimum Quantity</td>
                      <td>:</td>
                      <td>{{$product->minimum_qty}}</td>
                    </tr>
                    <tr>   
                      <td>Quantity</td>
                      <td>:</td>
                      <td>{{$product->qty}}</td>
                    </tr>
                    <tr>   
                      <td>Active</td>
                      <td>:</td>
                      <td>{{$product->active}}</td>
                    </tr>
                  </tbody>
                </table>
              </section>
            </div>
            <div class="col-sm-7">
              <section class="panel panel-default">
                <header class="panel-heading"><strong>Product Picture(s)</strong></header>
                <table class="table table-striped m-b-none">
                  <tbody>
                    <tr>                    
                      <td>
                        <div align="middle">
                          <img src="{{ $product->picture ? URL($product->picture) : '' }}" style="{{$product->picture ? '' : 'display:none;'}}" alt="product-image" class="image-products img-responsive img-thumbnail"/>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </section>
            </div>
          </div>
        </div>

        <div id="menu3" class="tab-pane fade">
          <section class="panel panel-default">
            <header class="panel-heading">
              {!! Form::open(['url' => '#', 'class' => 'form-horizontal','files' => true]) !!}
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-id-1">From Date</label>
                <div class="col-sm-2">
                  {{Form::text('from_date',null,['class' => 'form-control','id'=> 'from_date'])}}
                </div>
                <label class="col-sm-2 control-label" for="input-id-1">To Date</label>
                <div class="col-sm-2">
                {{Form::text('to_date',null,['class' => 'form-control','id'=> 'to_date'])}}
                </div>

                <div class="col-sm-4">
                  <a href="#" onclick="receiptMonthly(this, event)" class="btn btn-primary pull-right btn-sm">Filter</a>
                </div>
              </div>
              {{Form::close()}}
              <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
            </header>
            <div class="table-responsive">
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>TANGGAL</th>
                    <th>PO NUMBER</th>
                    <th>RECEIPT NUMBER</th>
                    <th>DESKRIPSI</th>
                    <th>QTY</th>
                    <th>PENGGUNA</th>
                  </tr>
                </thead>
                <tbody id="product-mountly" data-product="{{$product->id}}">
                  @if($product->receipt_items):
                    @foreach($product->receipt_items as $po)
                    <tr>
                      <td>{{$po->receipt_order->receipt_date}}</td>
                      <td>{{$po->receipt_order->po_number}}</td>
                      <td>{{$po->receipt_order->receipt_number}}</td>
                      <td>{{$po->product->name}}</td>
                      <td>{{$po->qty}}</td>
                      <td>{{$po->receipt_order->user->name}}</td>
                    </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </section> 
        </div>

        <div id="menu4" class="tab-pane fade">
          <section class="panel panel-default">
            <header class="panel-heading">
              {!! Form::open(['url' => '#', 'class' => 'form-horizontal','files' => true]) !!}
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-id-1">From Date</label>
                  <div class="col-sm-2">
                    {{Form::text('from_date',null,['class' => 'form-control','id'=> 'from_date1'])}}
                  </div>
                  <label class="col-sm-2 control-label" for="input-id-1">To Date</label>
                  <div class="col-sm-2">
                    {{Form::text('to_date',null,['class' => 'form-control','id'=> 'to_date1'])}}
                  </div>
                  <div class="col-sm-4">
                    <a href="#" onclick="sellMonthly(this, event)" class="btn btn-primary pull-right btn-sm">Filter</a>
                  </div>
                </div>
              {{Form::close()}}
              <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
            </header>
            <div class="table-responsive">
              <table class="table table-striped m-b-none">
                <thead>
                  <tr>
                    <th>TANGGAL</th>
                    <th>SALE NUMBER</th>
                    <th>DESKRIPSI</th>
                    <th>CUSTOMER</th>
                    <th>QTY</th>
                  </tr>
                </thead>
                <tbody id="product-mountly-sell" data-product="{{$product->id}}">
                  @if($product->sales_orders):
                    @foreach($product->sales_order_items as $so)
                    <tr>
                      <td>{{$so->sales_order->sales_date}}</td>
                      <td>{{$so->sales_order->sales_number}}</td>
                      <td>{{$so->product->name}}</td>
                      <td>{{$so->sales_order->customer->full_name}}</td>
                      <td>{{$so->qty}}</td>
                    </tr>
                    @endforeach
                  @endif;
                </tbody>
              </table>
            </div>
          </section> 
        </div>
      </div>
    </div>
  </section>
</section>
@endsection