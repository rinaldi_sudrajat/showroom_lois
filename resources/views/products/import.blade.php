@extends('layouts.default')
@section('content')
<section class="panel panel-default">
    <header class="panel-heading font-bold">
	Import Product
    </header>
    <div class="panel-body">
    {!! Form::open(['url' => '/proses_import', 'class' => 'form-horizontal','files' => true]) !!}
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Files</label>
        	<div class="col-sm-4">
              <input type="file" id="excel" name="excel" calss="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
        </div>
        </div>
    <div class="line line-dashed b-b line-lg pull-in"></div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-4">
            {!! Form::submit('Import', ['class' => 'btn btn-sm btn-info pull-right','id' => 'submit']) !!}
        </div>
    </div>
    </div>
    {{Form::close()}}
    </div>
</section>	
@endsection