@section('content')
  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-id-1">Type</label>
    <div class="col-sm-4">
      {{Form::text('product_type_id',null,['class' => 'form-control','id'=> 'product_type_id','required'=>true, 'onfocus'=>"dataAutocomplete(this,'/product-types')"])}}
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-id-1">Size Number</label>
    <div class="col-sm-4">
      {{Form::text('product_size_number_id',null,['class' => 'form-control','id'=> 'product_size_number_id','required'=>true, 'onfocus'=>"dataAutocomplete(this,'/product-number')"])}}
    </div>
  </div>
@endsection