@extends('layouts.default')
@section('content')
<section class="scrollable padder">
  <section class="panel panel-default">
<header class="panel-heading">
  Data Products 
  <a href="{{ URL('products/create') }}" class="btn btn-primary pull-right btn-sm">Add Product</a> <a href="{{URL('import_product')}}" class="btn btn-info pull-right btn-sm">Import Product</a><br>
  <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
</header>
<div class="table-responsive" style="
    overflow-y: auto;">
  <table id="data-products"  class="table table-striped m-b-none" data-ride="datatables">
    <thead>
      <tr>
        <th>Artikel</th>
        <th>Qty</th>
        <th>Sale</th>
        <th>Category</th>
        <th>Type</th>
        <th>Size</th>
        <th>Size Number</th>
        <th>View</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
</section>
</section>
@endsection