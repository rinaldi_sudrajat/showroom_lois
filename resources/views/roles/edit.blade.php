@extends('layouts.default') 
@section('content')
<section class="panel panel-default">
    <header class="panel-heading font-bold">
                  Edit Roles
</header>
    <div class="panel-body">
         {!! Form::open(['url' => route('roles.update', $role->id), 'method' => 'put', 'class' => 'form-horizontal','files' => true]) !!}
    <div class="form-group">
        <label class="col-sm-2 control-label" for="input-id-1">Name</label>
        	<div class="col-sm-4">
                {!! Form::text('name', $role->name, array('placeholder' => 'Name','class' => 'form-control')) !!}
              </div>
        </div>
		
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Display Name</label>
          <div class="col-sm-2">
{!! Form::text('display_name', $role->display_name, array('placeholder' => 'Display Name','class' => 'form-control')) !!}
              </div>
        </div>

         <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Description</label>
          <div class="col-sm-6">
                {!! Form::textarea('description', $role->description, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
              </div>
        </div>

         <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Permission</label>
          <div class="col-sm-6">
               @foreach($permission as $value)
                	<label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                	{{ $value->display_name }}</label>
                	<br/>
                @endforeach
            </div>
        </div>
         <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
        <div class="col-sm-offset-2 col-sm-4">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control','id' => 'submit']) !!}
        </div>
    </div>
	</div>
	{!! Form::close() !!}
    </div>
</section>
@endsection