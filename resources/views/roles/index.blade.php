@extends('layouts.default')
@section('content')
@include('flash::message')
<section class="scrollable padder">
  <section class="panel panel-default">
    <header class="panel-heading">
      Magagement Roles
        <a href="{{ URL('roles/create') }}" class="btn btn-primary pull-right btn-sm">Add Roles</a><br> 
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
        <div class="table-responsive">
        <table id="data-roles" class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Display Name</th>
                        <th>Description</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
@endsection