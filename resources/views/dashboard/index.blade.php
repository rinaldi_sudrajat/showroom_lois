@extends('layouts.default')
@section('content')
<section class="vbox">
  <section class="scrollable padder">              
    <section class="row m-b-md">
      <div class="col-sm-6">
        <h3 class="m-b-xs text-black">Dashboard</h3>
        <small>Welcome back, Admin</small>
      </div>
    </section>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel b-a">
          <div class="row m-n">
            <div class="col-md-6 b-b b-r">
              <a href="{{URL('products')}}" class="block padder-v hover">
                <span class="i-s i-s-2x pull-left m-r-sm">
                  <i class="i i-hexagon2 i-s-base text-danger hover-rotate"></i>
                  <i class="i i-plus2 i-1x text-white"></i>
                </span>
                <span class="clear">
                  <span class="h3 block m-t-xs text-danger">{{$count_product}}</span>
                  <small class="text-muted text-u-c">Products</small>
                </span>
              </a>
            </div>
            <div class="col-md-6 b-b">
              <a href="{{URL('customers')}}" class="block padder-v hover">
                <span class="i-s i-s-2x pull-left m-r-sm">
                  <i class="i i-hexagon2 i-s-base text-success-lt hover-rotate"></i>
                  <i class="i i-users2 i-sm text-white"></i>
                </span>
                <span class="clear">
                  <span class="h3 block m-t-xs text-success">{{$count_costumers}}</span>
                  <small class="text-muted text-u-c">Customers</small>
                </span>
              </a>
            </div>
            <div class="col-md-6 b-b b-r">
              <a href="#" class="block padder-v hover">
                <span class="i-s i-s-2x pull-left m-r-sm">
                  <i class="i i-hexagon2 i-s-base text-info hover-rotate"></i>
                  <i class="i i-location i-sm text-white"></i>
                </span>
                <span class="clear">
                  <span class="h3 block m-t-xs text-info">{{$count_sales}}</span>
                  <small class="text-muted text-u-c">Sales Order</small>
                </span>
              </a>
            </div>
            <div class="col-md-6 b-b b-r">
              <a href="#" class="block padder-v hover">
                <span class="i-s i-s-2x pull-left m-r-sm">
                  <i class="i i-hexagon2 i-s-base text-info hover-rotate"></i>
                  <i class="i i-location i-sm text-white"></i>
                </span>
                <span class="clear">
                  <span class="h3 block m-t-xs text-info">{{$count_receipt}}</span>
                  <small class="text-muted text-u-c">Good Receipt</small>
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
@endsection