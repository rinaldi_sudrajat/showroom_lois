@extends('layouts.default')
@section('content')
<section class="scrollable padder">
  <section class="panel panel-default">
    <header class="panel-heading">
      Data Category
    </header>
    <div class="table-responsive">
    <table id="data-category" class="table table-striped m-b-none">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    </div>
  </section>
</section>
@endsection