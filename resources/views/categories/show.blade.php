@extends('layouts.default')
@section('content')
<section class="vbox">
  <section class="scrollable padder">
    <div class="m-b-md">
      <h3 class="m-b-none">
        <a href="{{ URL('categories') }}" class="btn btn-primary"><i class="i i-circleleft text"></i>&nbsp; Back</a>
      </h3>
      <br>
      <header class="panel-heading">
      Data Product by category <b>{{$category->name}}</b>
    </header>
    <section class="panel panel-default">
      <div class="table-responsive">
        <table class="table table-striped b-t b-light">
          <thead>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th>Size</th>
              <th>Size Number</th>
              <th>Qty</th>
            </tr>
          </thead>
          <tbody>
            @foreach($category->product as $products)
              <tr>
                <td><a href="{{route('products.show', $products->id)}}">{{$products->name}}</a></td>
                <td>{{$products->type->name}}</td>
                <td>{{$products->size->name}}</td>
                <td>{{$products->number->name}}</td>
                <td>{{$products->qty}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
</section>
</section>
@endsection