@extends('layouts.default')
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<section class="panel panel-default">
	<header class="panel-heading font-bold">
                  Edit Costumer
</header>
    <div class="panel-body">
    {!! Form::open(['url' => route('customers.update', $data->id), 'method' => 'put', 'class' => 'form-horizontal','files' => true]) !!}
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Name</label>
        	<div class="col-sm-4">
              {{Form::text('full_name',$data->full_name,['class' => 'form-control','id'=> 'full_name'])}}
              </div>
          <input type="hidden" name="active" value="1">
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Address</label>
          <div class="col-sm-6">
              {{Form::textarea('address',$data->address,['class' => 'form-control','id'=>'address'])}}
              </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Phone</label>
          <div class="col-sm-4">
              {{Form::text('phone',$data->phone,['class' => 'form-control','id'=>'numbersonly','onkeypress'=>'return isNumber(event)'])}}
              </div>
        </div>
         <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
        <div class="col-sm-offset-2 col-sm-4">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control',
                                        'id' => 'submit']) !!}
        </div>
    </div>
    </div>
    {{Form::close()}}
    </div>
</section>	

@endsection