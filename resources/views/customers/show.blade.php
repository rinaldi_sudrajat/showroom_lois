@extends('layouts.default')
@section('content')
<section class="vbox">
  <section class="scrollable padder">
    <div class="m-b-md">
      <h3 class="m-b-none">
        <a href="{{ URL('customers') }}" class="btn btn-primary"><i class="i i-circleleft text"></i>&nbsp; Back</a>
      </h3>
      <br>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Costumer Detail</a></li>
    <li><a data-toggle="tab" href="#menu1">History Order</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="clearfix"></div>
    <section class="panel panel-default">
      <div class="table-responsive">
        <table class="table table-striped b-t b-light">
          <tbody>
            <tr>
              <td>Costumer ID</td>
              <td>
                <input class="form-control" id="disabledInput" type="text" name="place" placeholder="Content Here" value="{{$customers->customer_number}}" disabled>
              </td>
            </tr>
            <tr>
              <td>Name</td>
              <td>
                <input class="form-control" id="disabledInput" type="text" name="place" placeholder="Content Here" value="{{$customers->full_name}}" disabled>
              </td>
            </tr>
            <tr>
              <td>Address</td>
              <td>
                <input class="form-control" id="disabledInput" type="text" name="place" placeholder="Content Here" value="{{$customers->address}}" disabled>
              </td>
            </tr>
            <tr>
              <td>Phone</td>
              <td>
                <input class="form-control" id="disabledInput" type="text" name="place" placeholder="Content Here" value="{{$customers->phone}}" disabled>
              </td>
            </tr>
            <tr>
              <td>Point</td>
              <td>
                <input class="form-control" id="disabledInput" type="text" name="place" placeholder="Content Here" value="{{$customers->point_rewards}}" disabled>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>
    </div>
    <div id="menu1" class="tab-pane fade">
      <div class="clearfix"></div>
    <section class="panel panel-default">
    <header class="panel-heading">
          {!! Form::open(['url' => '/report_by_customer', 'class' => 'form-horizontal','files' => true]) !!}
        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-id-1">From Date</label>
        <div class="col-sm-2">
            {{Form::text('from_date',null,['class' => 'form-control','id'=> 'from_date'])}}
          </div>
        <label class="col-sm-2 control-label" for="input-id-1">To Date</label>
          <div class="col-sm-2">
            {{Form::text('to_date',null,['class' => 'form-control','id'=> 'to_date'])}}
          </div>

      <div class="col-sm-4">
            <a href="#" onclick="customerMonthly(this, event)" class="btn btn-primary pull-right btn-sm">Filter</a>
      </div>
    {{Form::close()}}
    <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
  </header>
      <div class="table-responsive">
        <table class="table table-striped b-t b-light">
          <thead>
            <tr>
              <th># Order</th>
              <th>Order Date</th>
              <th>Discount</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody id="customer-order" data-customers="{{$customers->id}}">
            @foreach($customers->sales_orders as $customer_order)
              <tr>
                <td>{{$customer_order->sales_number}}</td>
                <td>{{$customer_order->sales_date}}</td>
                <td>{{number_format($customer_order->customer_value)}}</td>
                <td>{{$customer_order->grand_total()}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
    </div>
</div>
</div>
</section>
</section>
@endsection