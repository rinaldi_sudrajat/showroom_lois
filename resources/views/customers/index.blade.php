@extends('layouts.default')
@section('content')
@include('flash::message')
<section class="scrollable padder">
  <section class="panel panel-default">
    <header class="panel-heading">
      Data Costumer Admin
        <a href="{{ URL('customers/create') }}" class="btn btn-primary pull-right btn-sm">Add Costumer</a><br> 
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
                </header>
        <div class="table-responsive" style="overflow-y: auto;">
        <table id="data-customers" style="/* width: 1054px; */min-width: 1800px;" class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th>Custumer ID</th>
                        <th>Full Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Point Reward</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </section>
            </section>
@endsection