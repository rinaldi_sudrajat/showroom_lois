@extends('layouts.default')
@section('content')
  @foreach($trans as $so )
    <tr>
       <td>{{$so->sales_order->sales_number}}</td>
       <td>{{$so->sales_order->sales_date}}</td>
       <td>{{number_format($so->sales_order->customer_value)}}</td>
       <td>{{$so->sales_order->grand_total()}}</td>
    </tr>
  @endforeach
@endsection