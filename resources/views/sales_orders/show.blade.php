<style>
@media print{
          body {margin-top: 0.08mm; margin-bottom: 2.79mm; 
           margin-left: 0.15mm; margin-right: 0.15mm;}
          .hidden-print,
          .page-bar,
          .page-footer,
          .page-quick-sidebar-wrapper,
          .page-sidebar-wrapper{
          display: none;
          font-family: "Georgia";
}
    @page {
                size: 24.13cm 27.94cm;
    }
}
</style>
<body onload="window.print()">
<div class="clearfix"></div>
<table align="center" border=0 width="97%" style="margin-top:5px;  border:1px solid black;">
  <tr>
    <td style="padding: 2px; font-size: 24px; font-family: Georgia,Times,Times New Roman,serif;"><strong>SHOWROOM BANDUNG<strong></td>
    <td>
    <td width="25%" style="padding: 2px; font-size: 17px; font-family: Georgia,Times,Times New Roman,serif;">Sellout date</td>
    <td style="padding: 2px; font-size: 17px; font-family: Georgia,Times,Times New Roman,serif;">:&nbsp;{{$sales_order->sales_date}}</td>
    </td>
  </tr>
  <tr>
    <td style="padding: 2px; font-size: 13px;font-family: Georgia, Times, "Times New Roman", serif;">Alamat</td>
    <td></td>
    <td width="20%" style="padding: 2px; font-size: 17px; font-family: Georgia,Times,Times New Roman,serif;">Customer</td>
    <td width="20%" style="padding: 2px; font-size: 17px; font-family: Georgia,Times,Times New Roman,serif;"><b>:&nbsp;{{empty($sales_order->customer->name) ? '-' : $sales_order->customer->name}}</b></td>
  </tr>
  <tr>

  </tr>
    <tr>
    <td width="25%" style="padding: 2px; font-size: 17px;">Deskripsi</td>
    <td style="padding: 2px; font-size: 17px; font-family: Georgia, Times, "Times New Roman", serif;">:&nbsp;{!! $sales_order->description !!}</td>
        <td width="20%" style="padding: 2px; font-size: 17px; font-family: Georgia,Times,Times New Roman,serif;">Number Sell out</td>
    <td width="20%" style="padding: 2px; font-size: 17px; font-family: Georgia,Times,Times New Roman,serif;"><b>:&nbsp;{{$sales_order->sales_number}}</b></td>
    <td></td>

  </tr>
</table>

<table align="center" border=6 width="97%" style="margin-top:5px; border:1px solid black;">
  <tr>
    <td style="padding: 5px; font-weight: bold; font-size: 15px; font-family: Georgia, Times, "Times New Roman", serif;" align="center">Quantity</td>
    <td style="padding: 5px; font-weight: bold; font-size: 15px; font-family: Georgia, Times, "Times New Roman", serif;" align="center">Product Name</td>
  </tr>
  @foreach($sales_order->order_items as $item)
  <tr>
    <td  style="padding: 5px; font-size: 16px; border:1px solid black; font-family: Georgia, Times, "Times New Roman", serif;" align="center">{{$item->qty}}</td>
    <td  style="padding: 5px;font-size: 16px;border:1px solid black;font-family: Georgia, Times,;word-spacing: 7px;">{{$item->product->name}}</td>
  @endforeach
  </tr>
</table>

<table border=1 align="center" width="97%" style="margin-top:5px; border:1px solid black;">
  <tr>
    <td valign="top" width="100%">
      <table  border=0 width="100%">
        <tr>
          <td style="font-size: 13px;" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total</td>
          <td>:</td>
          <td style="font-size: 17px; font-family: Georgia, Times, "Times New Roman", serif;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp&nbsp;{{number_format($sales_order->grand_total(),0,'.',',')}}</b></td>
        </tr>

        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>

        <tr>
          <table border=0 width="100%">
            <tr>
              <td style="font-family: Georgia, Times, "Times New Roman", serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hormat Kami</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="font-family: Georgia, Times, "Times New Roman", serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                ( {{$sales_order->user->name}} )
              </td>
            </tr>
          </table>
        </tr>
      </table>
    </td>
  </tr>
</table>
<script type="text/javascript">
  $('.editable').each(function(){
    this.contentEditable = true;
});
</script>