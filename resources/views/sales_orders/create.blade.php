@extends('layouts.default')
@section('content')
<div class="row">
  <div class="col-sm-12">
    {!! Form::open(['url' => 'sales_orders', 'class' => 'form-horizontal',"data-validate"=>"parsley","id"=>"order_create", "onsubmit"=>"submitSalesOrder(this, event)"]) !!}
      <section class="panel panel-default">
        <header class="panel-heading">
          <strong>Sales Orders</strong>
        </header>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-3 control-label">Date</label>
            <div class="col-sm-9">
              {{Form::text('sales_date', null, ['class'=>'form-control','data-notblank'=>"true","id"=>"from_date"])}}
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 control-label">Customer</label>
            <div class="col-sm-6">
              {{Form::select('customer_id',$customer,null,['id'=>'customer_id','class' => 'form-control chosen-select','placeholder'=>'Select Customer'])}}
            </div>
          </div>
          
          <div class="form-group text-right bg-light lter">
                <div class="col-sm-2"></div>
                <label class="col-sm-1 checkbox i-checks"> 
                  <input type="checkbox" name="inlineCheckbox10" id="checkboxTax" value="0" onclick="calcTaxMember(this)" onchange="CheckBoxCustomer(this)"><i></i> Disc 10%
                </label>
                <div class="col-sm-3">
                  {{Form::text('customer_discount',0,['class' => 'form-control payments-bottom','required'=>true, 'disabled'=>true, 'id'=>'taxMember'])}}
                </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 control-label">Description</label>
            <div class="col-sm-9">
              {{Form::textarea('description', null, ['rows'=>2, 'class'=>'form-control', 'id' => 'description'])}}
            </div>
          </div>
        
          <div class="form-group">
            <label class="col-sm-3 control-label">Order By</label>
            <div class="col-sm-9">
              {{Form::text('user', Auth::user()->name, ['class' => 'form-control','placeholder'=>'Order By','required'=>true, 'disabled'=>true])}}
            </div>
          </div>

      <div class="form-group">
        <label class="col-sm-3 control-label">Payment</label>
        <div class="col-sm-9">
          {{Form::select('payment_id',["1"=>"Cash","2"=>"Card"],null,['id'=>'payment_id','class' => 'form-control chosen-select','required'=>true,'onchange'=>'setPayment(this, event)'])}}
        </div>
      </div>

      <div id="div_brand" class="form-group" style="display:none;">
      <label class="col-sm-3 control-label">Type EDC</label>
      <div class="col-sm-9">
        {{Form::text('edc_payment_id',null,['class' => 'form-control','id'=> 'edc_payment_id','onfocus'=>"dataAutocomplete(this,'/payment-types')"])}}
      </div>
    </div>

          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <div class="col-sm-12 table-responsive">
              <table id="data-orders" class="table table-striped m-b-none" data-ride="datatables">
                <thead>
                  <tr><center>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Subtotal</th>
                    <th></th>
                    </center>
                  </tr>
                </thead>
                <tbody id="orderItem">
                </tbody>
                <tr>
                  <td colspan="9">
                  <a href="#" class="btn btn-success" onclick="loadProductSales();">Add product</a></td>
                </tr>
              </table>
            </div>
          </div>

          <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group text-right bg-light lter">
                <div class="col-sm-6"></div>
                <label class="col-sm-3 control-label">Total</label>
                <div class="col-sm-3">
                  {{Form::text('total',0,['class' => 'form-control payments-bottom','required'=>true, 'disabled'=>true, 'id'=>'transact_total'])}}
                </div>
              </div>
              <div class="form-group text-right bg-light lter">
                <div class="col-sm-3"></div>
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-6">
                  {{Form::hidden('discount_value',0,['class' => 'form-control payments-bottom','required'=>true, 'disabled'=>true,'id'=>'discount_value', 'value'=>0])}}
                </div>
              </div>
              <div class="form-group text-right bg-light lter">
                <div class="col-sm-6"></div>
                <label class="col-sm-3 checkbox i-checks">
                  <input type="checkbox" name="inlineCheckbox1" id="checkbox20" value="0" onclick="calcTaxTrans(this)"><i></i> Disc 20%
                </label>
                <div class="col-sm-3">
                  {{Form::text('tax',0,['class' => 'form-control payments-bottom','required'=>true, 'disabled'=>true, 'id'=>'taxTrans'])}}
                </div>
              </div>
              <div class="form-group text-right bg-light lter">
                <div class="col-sm-6"></div>
                <label class="col-sm-3 control-label">Grand Total</label>
                <div class="col-sm-3">
                  {{Form::text('grand_total',0,['class' => 'form-control payments-bottom','required'=>true, 'disabled'=>true,'id'=>'grand_total'])}}
                </div>
              </div>
            </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>
        <footer class="panel-footer text-right bg-light lter">
          <button type="submit" class="btn btn-success btn-s-xs" id="button">Submit</button>
        </footer>
        </section>
{{Form::close()}}
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Product</h4>
          </div>
          <div class="modal-body" id="list-productOrder">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="getCheckedBoxesSales('checkboxName')">Add</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Customer modal -->
    <div class="modal fade" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Customer</h4>
          </div>
          <div class="modal-body" id="customer-form">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Service Order -->
    <div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Service</h4>
          </div>
          <div class="modal-body" id="service-form">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="markService('checkboxName')">Add</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection