@section('content')
<section class="scrollable padder">
  <section class="panel panel-default">
    <header class="panel-heading">
      Data Products 
    </header>
    <div class="table-responsive">
      <table class="table table-striped m-b-none product-po" data-ride="datatables">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Category</th>
            <th>Size</th>
            <th>Size Number</th>
            <th>Qty</th>
          </tr>
        </thead>
        <tbody>
          @foreach($products as $product)
            <tr id="product_{{$product->id}}">
              <td>
                <input type="checkbox" data-id="{{$product->id}}" class="checkboxName" value="#product_{{$product->id}}" data-required="true"><i></i>
              </td>
              <td>{{$product->name}}</td>
              <td>{{$product->category->name}}</td>
              <td>{{$product->size->name}}</td>
              <td>{{$product->number->name}}</td>
              <td>{{$product->qty}}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </section>
</section>
@endSection