@extends('layouts.default')
@section('content')
  <section class="scrollable padder">
    <section class="panel panel-default">
      <header class="panel-heading">
        Data Sales Orders
        <a href="{{ URL('sales_orders/create') }}" class="btn btn-primary pull-right btn-sm">Add Order</a><br>
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
      </header>
      <div class="table-responsive">
        <table id="data-sales" class="table table-striped m-b-none" data-ride="datatables">
          <thead>
            <tr>
              <th>Order number</th>
              <th>Date</th>
              <th>Customer</th>
              <th>Order by</th>
              <th>Total</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($sales_orders as $order)
            <tr>
              <td>{{$order->sales_number}}</td>
              <td>{{$order->sales_date}}</td>
              <td>{{$order->customer->full_name}}</td>
              <td>{{$order->user->name}}</td>
              <td>Rp. {{number_format($order->grand_total())}}</td>
              <td><a class='btn btn-info btn-sm' href="{{ URL('sales_orders', $order->id) }}">View</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
  </section>
@endsection