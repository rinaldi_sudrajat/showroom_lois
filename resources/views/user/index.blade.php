@extends('layouts.default')
@section('content')
  <section class="scrollable padder">
    <section class="panel panel-default">
      <header class="panel-heading">
        Data User
        <a href="{{ URL('users/create') }}" class="btn btn-primary pull-right btn-sm">Add User</a><br>
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
      </header>
      <div class="table-responsive">
        <table id="data-users" class="table table-striped m-b-none" data-ride="datatables">
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Email</th>
              <th>Roles</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($data as $key=> $item)
            <tr>
              <td>{{$item->id}}</td>
              <td>{{$item->name}}</td>
              <td>{{$item->email}}</td>
              <td>@if(!empty($item->roles))
			  	  @foreach($item->roles as $v)
					<label class="label label-success">{{ $v->display_name }}</label>
				    @endforeach
			      @endif</td>
              <td><a class="btn btn-info btn-xs" href="{{ route('users.edit',$item->id) }}">Edit</a>
                  {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $item  ->id],'style'=>'display:inline']) !!}
                  {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                  {!! Form::close() !!}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
  </section>
@endsection