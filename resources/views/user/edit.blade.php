@extends('layouts.default')
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<section class="panel panel-default">
    <header class="panel-heading font-bold">Edit Users</header>
    <div class="panel-body">
    {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id], 'class' => 'form-horizontal']) !!}
    <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Name</label>
            <div class="col-sm-4">
                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
              </div>
    </div>
    <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Email</label>
          <div class="col-sm-6">
                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
              </div>
    </div>

    <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Password</label>
          <div class="col-sm-6">
                {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
              </div>
    </div>

    <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Confirm Password</label>
          <div class="col-sm-6">
                {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
              </div>
    </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Roles</label>
          <div class="col-sm-6">
                {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')) !!}
              </div>
    </div>

             <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
        <div class="col-sm-offset-2 col-sm-4">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control',
                                        'id' => 'submit']) !!}
        </div>
    </div>
</section>
@endsection