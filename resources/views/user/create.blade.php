@extends('layouts.default')
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<section class="panel panel-default">
	<header class="panel-heading font-bold">
                  Insert User
</header>
    <div class="panel-body">
    {!! Form::open(['url' => 'users', 'class' => 'form-horizontal','files' => true]) !!}
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Name</label>
        	<div class="col-sm-4">
              {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
              </div>
          <input type="hidden" name="active" value="1">
        </div>

           <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Email</label>
          <div class="col-sm-4">
              {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
              </div>
          <input type="hidden" name="status" value="1">
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Password</label>
          <div class="col-sm-4">
              {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
              </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Confirm Password</label>
          <div class="col-sm-4">
               {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
              </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">Roles</label>
        	<div class="col-sm-4">
        {{Form::select('role_id',$roles,null,['class' => 'form-control m-t chosen-select','placeholder'=>'Select Role User','required'=>true])}}
        </div>

         <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
        <div class="col-sm-offset-2 col-sm-4">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control',
                                        'id' => 'submit']) !!}
        </div>
    </div>
    </div>
    {{Form::close()}}
    </div>
</section>	

@endsection