<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
</head>
<body>
<table border="1" cellpadding="3" cellspacing="0" width="100%">
    <tr>
        <th><img src="{{URL('admin/images/lois.jpg') }}" width="200" height="70" alt="picture"></th>
        <th colspan="6">Data Stock Products</th>
        {{-- <th colspan="6"></th> --}}
    </tr>
    <tr>
        <td colspan="6">&nbsp;</td>

    </tr>
    <tr>
        <th align="left">TOTAL</th>
        <th align="left">{{ $product->count() }}</th>
        <td colspan="4"></td>
    </tr>
    <tr>
        <th align="left">DOWNLOADED AT</th>
        <th align="left">{{ Carbon\Carbon::now() }}</th>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
        <th>No</th>
        <th>Artikel</th>
        <th>Category</th>
        <th>Qty</th>
        <th>Sale Price</th>
        <th>Total</th>
    </tr>
    <tbody>
        @php($no = 1)
        @foreach ($product as $items)
                  <tr>
                    <td align="center">{{ $no }}</td>
                    <td>{{$items->name}}</td>
                    <td>{{$items->category->name}}</td>
                    <td>{{$items->qty}}</td>
                    <td>{{number_format($items->sale_price)}}</td>
                    <td>{{number_format($items->qty * $items->sale_price )}}</td>
                  </tr>
            @php($no++)
        @endforeach
    </tbody>
</table>
</body>
</html>