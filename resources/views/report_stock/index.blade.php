@extends('layouts.default')
@section('content')
  <section class="scrollable padder">
    <section class="panel panel-default">
      <header class="panel-heading">
        Data Report Stock Product
        <a href="{{URL('export_product')}}" class="btn btn-primary pull-right btn-sm">Export Excel</a><br>
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i> 
      </header>
      <div class="table-responsive">
        <table id="data-report-sales" class="table table-striped m-b-none" data-ride="datatables">
          <thead>
            <tr>
              <th>Artikel</th>
              <th>Category</th>
              <th>Qty</th>
              <th>Sale Price</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            @foreach($product as $item)
            <tr>
              <td>{{$item->name}}</td>
              <td>{{$item->category->name}}</td>
              <td>{{$item->qty}}</td>
              <td>{{number_format($item->sale_price)}}</td>
              <td>{{number_format($item->qty * $item->sale_price)}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
  </section>
@endsection