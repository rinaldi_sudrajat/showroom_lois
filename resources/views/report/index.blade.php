@extends('layouts.default')
@section('content')
<section class="vbox">
  <section class="scrollable padder">
    <div class="container">
    	<h3>Report Sales Order Daily</h3>
	</div>  
    <section class="panel panel-default">
      <header class="panel-heading">
        {!! Form::open(['url' => '/report_so_by_range', 'class' => 'form-horizontal', 'method'=>'GET']) !!}
        <div class="form-group">
          <label class="col-sm-1 control-label" for="input-id-1">From</label>
          <div class="col-sm-2">
            {{Form::text('from_date',null,['class' => 'form-control','id'=> 'from_date'])}}
          </div>
          <label class="col-sm-1 control-label" for="input-id-1">To Date</label>
          <div class="col-sm-2">
            {{Form::text('to_date',null,['class' => 'form-control','id'=> 'to_date'])}}
          </div>
          <label class="col-sm-1 control-label" for="input-id-1">Payment</label>
          <div class="col-sm-2">
          {{Form::select('payment_id',$payments,null,['id'=>'payment_id','class' => 'form-control chosen-select','required'=>true])}}
          </div>
          <label class="col-sm-1 control-label" for="input-id-1">Category</label>
          <div class="col-sm-2">
          {{Form::select('category_id',$category,null,['id'=>'category_id','class' => 'form-control chosen-select','required'=>true])}}
          </div>
          <label class="col-sm-1 control-label" for="input-id-1"></label>
          <div class="col-sm-2">
            {!! Form::submit('Filter', ['class' => 'btn btn-primary btn-sm','id' => 'submit']) !!}
            <a href="#" data-uri="/print_data_report" onClick="printDataSellout(this,event)" class="btn btn-info btn-sm">Print</a>
          </div>
        </div>
        {{Form::close()}}
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
      </header>
      <div class="table-responsive">
        <table id="data-purchases" class="table table-striped m-b-none" data-ride="datatables">
          <thead>
            <tr>
              <th>Tanggal</th>
              <th>Qty</th>
              <th>Harga</th>
              <th>Disc Value</th>
              <th>Payment</th>
              <th>Total</th>
            </tr>
          </thead>
        <tfoot>
            <tr>
                <th colspan="5" style="text-align:right">Total:</th>
                <th>{{number_format($sales_order->sum(function ($items){
                  return ($items->qty * $items->price) - $items->customer_value - $items->discount_value;})) }}</th>
            </tr>
        </tfoot>
          <tbody>
            @foreach($sales_order as $items)
            <tr>
              <td>{{$items->sales_date}}</td>
              <td>{{$items->qty}}</td>
              <td>{{number_format($items->price)}}</td>
              @if($items->customer_value == 0)
                  <td>{{number_format($items->discount_value)}}</td>
              @else
                  <td>{{number_format($items->customer_value)}}</td>
              @endif
              <td>{{$items->payment_name}}</td>
              <td>{{number_format($items->qty * $items->price - $items->customer_value - $items->discount_value)}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </section>
  </section>
</section>
@endsection