@extends('layouts.default')
@section('content')
<section class="vbox">
  <section class="scrollable padder">
    <div class="container">
    	<h3>Report Sales Order Mounth / Years</h3>
	</div>  
    <section class="panel panel-default">
      <header class="panel-heading">
        {!! Form::open(['url' => '/report_mounth', 'class' => 'form-horizontal', 'method'=>'GET']) !!}
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-id-1">From Mounth</label>
          <div class="col-sm-2">
            {{Form::text('from_date',null,['class' => 'form-control','id'=> 'from_date'])}}
          </div>
          <div class="col-sm-2">
            {!! Form::submit('Filter', ['class' => 'btn btn-primary btn-sm','id' => 'submit']) !!}
          </div>
        </div>
        {{Form::close()}}
        <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data."></i>
      </header>
      <div class="table-responsive">
        <table id="data-purchases" class="table table-striped m-b-none" data-ride="datatables">
          <thead>
            <tr>
              <th>Total Qty</th>
              <th>Total Price</th>
            </tr>
          </thead>
          <tbody>
			@foreach($data_mounth as $value)
			<tr>
			<td>{{$value->total_qty}}</td>
			<td>{{number_format($value->total_price)}}</td>
			</tr>
			@endforeach
          </tbody>
        </table>
      </div>
    </section>
  </section>
</section>
@stop
