<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
</head>
<body>
<table border="1" cellpadding="3" cellspacing="0" width="100%">
    <tr>
        <th><img src="{{URL('admin/images/lois.jpg') }}" width="200" height="70" alt="picture"></th>
        <th colspan="6">Report Sales Order</th>
        {{-- <th colspan="6"></th> --}}
    </tr>
    <tr>
        <td colspan="6">&nbsp;</td>

    </tr>
    <tr>
        <th align="left">TOTAL</th>
        <th align="left">{{ $sales_order->count() }}</th>
        <td colspan="4"></td>
    </tr>
    <tr>
        <th align="left">DOWNLOADED AT</th>
        <th align="left">{{ Carbon\Carbon::now() }}</th>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
              <th>Tanggal</th>
              <th>Qty</th>
              <th>Harga</th>
              <th>Disc Value</th>
              <th>Payment</th>
              <th>Total</th>
    </tr>
    <tbody>
        @php($no = 1)
            @foreach($sales_order as $items)
            <tr>
              <td>{{$items->sales_date}}</td>
              <td>{{$items->qty}}</td>
              <td>{{number_format($items->price)}}</td>
              @if($items->customer_value == 0)
                  <td>{{number_format($items->discount_value)}}</td>
              @else
                  <td>{{number_format($items->customer_value)}}</td>
              @endif
              <td>{{$items->payment_name}}</td>
              <td>{{number_format($items->qty * $items->price - $items->customer_value - $items->discount_value)}}</td>
            </tr>
            @endforeach
    </tbody>
</table>
</body>
</html>